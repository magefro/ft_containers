//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_VECTOR_CONSTRUCTORS_HPP
#define FT_CONTAINERS_TEST_VECTOR_CONSTRUCTORS_HPP

#include "test_vector_tools.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

using namespace NAMESPACE;

void test_vector_constructors()
{
	std::cout << std::endl << "TEST VECTOR CONSTRUCTORS" << std::endl;
	NAMESPACE::vector<TESTED_TYPE> vector_default_1;
	print_vector_specs(vector_default_1);
	NAMESPACE::vector<TESTED_TYPE> vector_fill_1(5);
	print_vector_specs(vector_fill_1);
	NAMESPACE::vector<TESTED_TYPE> vector_fill_2(5, 42);
	print_vector_specs(vector_fill_2);
	NAMESPACE::vector<TESTED_TYPE> vector_range_1(vector_fill_2.begin(), vector_fill_2.end());
	print_vector_specs(vector_range_1);
	NAMESPACE::vector<TESTED_TYPE> vector_copy_1(vector_fill_2);
	print_vector_specs(vector_copy_1);
	NAMESPACE::vector<TESTED_TYPE> vector_assignation_1 = vector_copy_1;
	print_vector_specs(vector_assignation_1);
}

#endif //FT_CONTAINERS_TEST_VECTOR_CONSTRUCTORS_HPP

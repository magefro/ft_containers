//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_VECTOR_ITERATORS_HPP
#define FT_CONTAINERS_TEST_VECTOR_ITERATORS_HPP

#include "test_vector_tools.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

using namespace NAMESPACE;

void test_vector_iterators()
{
	std::cout << std::endl << "TEST VECTOR ITERATORS" << std::endl;
	NAMESPACE::vector<TESTED_TYPE> vector;
	for (size_t i = 0; i <= 42; i++)
		vector.push_back(i);
	print_vector_specs(vector);
	std::cout
		<< "begin: " << *vector.begin() << std::endl
		<< "++begin: " << *(++vector.begin()) << std::endl
		<< "--end: " << *(--vector.end()) << std::endl
		<< "rbegin: " << *vector.rbegin() << std::endl
		<< "++rbegin: " << *(++vector.rbegin()) << std::endl
		<< "--rend: " << *(--vector.rend()) << std::endl
		<< "begin + 4: " << *(vector.begin() + 4) << std::endl
		<< "end - 1: " << *(vector.end() - 1) << std::endl
		<< "begin() + 1: " << *(vector.begin() + 1) << std::endl
		<< "begin() + 2: " << *(vector.begin() + 2) << std::endl
		<< "begin() + 3: " << *(vector.begin() + 3) << std::endl
		<< "begin() + 4: " << *(vector.begin() + 4) << std::endl
		<< "begin() + 5: " << *(vector.begin() + 5) << std::endl
		<< "rbegin(): " << *(vector.rbegin()) << std::endl
		<< "rbegin() - 0: " << *(vector.rbegin() - 0) << std::endl
		<< "rbegin() + 0: " << *(vector.rbegin() + 0) << std::endl
		<< "rbegin() + 1:	" << *(vector.rbegin() + 1) << std::endl
		<< "rbegin() + 2:	" << *(vector.rbegin() + 2) << std::endl
		<< "rbegin() + 3:	" << *(vector.rbegin() + 3) << std::endl
		<< "rbegin() + 4:	" << *(vector.rbegin() + 4) << std::endl
		<< "rbegin() + 5:	" << *(vector.rbegin() + 5) << std::endl
		<< "rend   - 4:		" << *(vector.rend() - 4) << std::endl
		<< "rend   - 4:		" << (vector.rend() - vector.rbegin()) << std::endl
		<< "rend   - 4:		" << (vector.end() - vector.begin()) << std::endl;
	print_vector_specs(vector);
}

#endif //FT_CONTAINERS_TEST_VECTOR_ITERATORS_HPP

//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_VECTOR_TOOLS_HPP
#define FT_CONTAINERS_TEST_VECTOR_TOOLS_HPP

#include <vector>
#include "../../includes/vector.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

using namespace NAMESPACE;

template <typename T>
void print_vector_specs_iterators(NAMESPACE::vector<T> &vector)
{
	std::cout
			<< "begin: " << *vector.begin() << std::endl
			//<< "end: " << *vector.end() << std::endl
			<< "rbegin: " << *vector.rbegin() << std::endl
			<< "rend: " << *vector.rend() << std::endl;
}

template <typename T>
std::string capacity_checker(NAMESPACE::vector<T> &vector)
{
	if (vector.capacity() >= vector.size())
		return ("OK");
	else
		return ("FAILED");
}

template <typename T>
void print_vector_specs(NAMESPACE::vector<T> &vector, bool details = false)
{
	std::cout << "-----------------------------------------" << std::endl;
	std::cout
			<< "VECTOR SPECS" << std::endl
			<< "size: " << vector.size() << std::endl
			<< "max_size: " << vector.max_size() << std::endl
			<< "capacity: " << capacity_checker(vector) << " " << vector.capacity() << std::endl;

	if (details == true)
		print_vector_specs_iterators(vector);

	std::cout << "data: ";
	for (size_t i = 0; i < vector.size(); i++)
	{
		std::cout << vector[i];
		if (i != vector.size() - 1)
			std::cout << ", ";
	}
	std::cout << std::endl;
	std::cout << "-----------------------------------------" << std::endl;

}

#endif //FT_CONTAINERS_TEST_VECTOR_TOOLS_HPP

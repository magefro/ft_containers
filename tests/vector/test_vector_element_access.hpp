//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_VECTOR_ELEMENT_ACCESS_HPP
#define FT_CONTAINERS_TEST_VECTOR_ELEMENT_ACCESS_HPP

#include "test_vector_tools.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

using namespace NAMESPACE;

void test_vector_element_access()
{
	std::cout << std::endl << "TEST VECTOR ELEMENT ACCESS" << std::endl;
	NAMESPACE::vector<TESTED_TYPE> vector;
	NAMESPACE::vector<TESTED_TYPE> vector_empty;
	for (int i = 0; i < 42; i++)
	{
		vector.push_back(i);
	}
	std::cout
		<< "front: " << vector.front() << std::endl
		<< "back: " << vector.back() << std::endl
		//	<<	"font:	" << vector_empty.front()	<< std::endl	SEGFAULTS :)
		//	<<	"back:	" << vector_empty.back()	<< std::endl	SEGFAULTS :)
		//	<<	"[]:	" << *vector.at()		<< std::endl // Already tested in print_vector_specs
		;
	try
	{
		std::cout << "at: " << vector.at(12) << std::endl;
		std::cout << "at: " << vector.at(0) << std::endl;
		std::cout << "at: " << vector.at(200) << std::endl;
	}
	catch (const std::exception &e)
	{
		std::cerr << "error found with .at(): " << e.what() << std::endl;
	}
	print_vector_specs(vector);
}

#endif //FT_CONTAINERS_TEST_VECTOR_ELEMENT_ACCESS_HPP

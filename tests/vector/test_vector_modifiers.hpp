//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_VECTOR_MODIFIERS_HPP
#define FT_CONTAINERS_TEST_VECTOR_MODIFIERS_HPP

#include "test_vector_tools.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

using namespace NAMESPACE;

void test_vector_modifiers()
{
	std::cout << std::endl << "TEST VECTOR MODIFIERS" << std::endl;
	NAMESPACE::vector<TESTED_TYPE> vector;
	for (int i = 0; i < 42; i++)
	{
		vector.push_back(i);
	}
	print_vector_specs(vector);
	NAMESPACE::vector<TESTED_TYPE> vector2(vector);
	std::cout << "ASSIGN" << std::endl;
	vector.assign(42, 42);
	print_vector_specs(vector);
	vector.assign(44, 44);
	print_vector_specs(vector);
	vector.assign(42, 42);
	print_vector_specs(vector);
	vector.assign(1, 1);
	print_vector_specs(vector);
	vector.assign(0, 0);
	print_vector_specs(vector);
	vector.assign(400, 666);
	print_vector_specs(vector);
	vector.assign(42, 42);
	print_vector_specs(vector);
	vector.assign(vector.begin(), vector.end());
	print_vector_specs(vector);
	vector2.assign(vector.begin(), vector.end());
	print_vector_specs(vector2);
	vector2.assign(vector.begin(), vector.begin());
	print_vector_specs(vector2);
	std::cout << "PUSH BACK" << std::endl;
	vector.clear();
	vector.push_back(1);
	print_vector_specs(vector);
	vector.push_back(2);
	print_vector_specs(vector);
	vector.push_back(3);
	print_vector_specs(vector);
	vector.pop_back();
	print_vector_specs(vector);
	vector.pop_back();
	print_vector_specs(vector);
	vector.pop_back();
	print_vector_specs(vector);
	vector.clear();
	for (int i = 0; i < 42; i++)
		vector.push_back(i);
	print_vector_specs(vector);
	std::cout << "ERASE" << std::endl;
	vector.erase(vector.begin() + 2);
	print_vector_specs(vector);
	vector.erase(vector.begin());
	print_vector_specs(vector);
	vector.erase(vector.begin() + 16, vector.begin() + 28);
	print_vector_specs(vector);
	vector.erase(vector.begin(), vector.end() - 2);
	print_vector_specs(vector);
	vector.erase(vector.end() - 4, vector.end() - 4);
	print_vector_specs(vector, true);
	std::cout << "INSERT" << std::endl;
	vector.clear();
	vector2.clear();
	for (int i = 0; i < 42; i++)
		vector2.push_back(i);
	std::cout << "vector: " << std::endl;
	print_vector_specs(vector);
	std::cout << "vector2: " << std::endl;
	print_vector_specs(vector2);
	std::cout << "break 1: " << std::endl;
	vector2.insert(vector2.begin() + 4, 666);
	print_vector_specs(vector);
	std::cout << "break 2: " << std::endl;
	vector2.insert(vector2.begin(), 666);
	print_vector_specs(vector2);
	std::cout << "break 3: " << std::endl;
	vector.insert(vector.begin(), vector2.begin(), vector2.end());
	print_vector_specs(vector);
	std::cout << "break 4" << std::endl;
	vector.insert(vector.begin(), vector2.begin() + 2, vector2.end() - 4);
	print_vector_specs(vector);
	std::cout << "break 5" << std::endl;
	vector.insert(vector.begin(), 5, 1);
	print_vector_specs(vector);
	vector.clear();
	std::cout << "break 6" << std::endl;
	vector.insert(vector.end(), 5, 2);
	print_vector_specs(vector);
	vector.clear();
	std::cout << "break 7" << std::endl;
	vector.insert(vector.begin(), 0, 3);
	print_vector_specs(vector);
	vector.clear();
	NAMESPACE::vector<TESTED_TYPE> vector_default_1(1, 11);
	NAMESPACE::vector<TESTED_TYPE> vector_default_2(2, 22);
	vector_default_1.swap(vector_default_2);
	print_vector_specs(vector_default_1, true);
	print_vector_specs(vector_default_2, true);
	vector_default_2.swap(vector_default_1);
	print_vector_specs(vector_default_1, true);
	print_vector_specs(vector_default_2, true);
	try
	{
		vector_default_1.swap(vector_default_1);
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << '\n';
	}
	print_vector_specs(vector_default_1, true);
	print_vector_specs(vector_default_2, true);
}

#endif //FT_CONTAINERS_TEST_VECTOR_MODIFIERS_HPP

//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_VECTOR_OTHER_HPP
#define FT_CONTAINERS_TEST_VECTOR_OTHER_HPP

#include "test_vector_tools.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

using namespace NAMESPACE;

void test_vector_other()
{
	std::cout << "VECTOR EXCEPTION SAFETY" << std::endl;
	NAMESPACE::vector<TESTED_TYPE> vector_default_1;
	vector_default_1.push_back(1);
	vector_default_1.push_back(2);
	vector_default_1.push_back(3);
	try
	{
		vector_default_1.reserve(-1);
	}
	catch (const std::exception &e)
	{
		std::cerr << "error found: " << e.what() << '\n';
	}
	print_vector_specs(vector_default_1);
	NAMESPACE::vector<TESTED_TYPE> vec; // data NULL
	try
	{
		vec.insert(vec.begin(), 5, 42);
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << '\n';
	}
	print_vector_specs(vector_default_1);
	std::cout << "VECTOR ERRORS" << std::endl;
	vector_default_1.clear(); // default
	vector_default_1.push_back(1);
	vector_default_1.push_back(2);
	vector_default_1.push_back(3);
	print_vector_specs(vector_default_1);
	vector_default_1.push_back(1);
	vector_default_1.push_back(2);
	vector_default_1.push_back(3);
	try
	{
		vector_default_1.resize(-4);
	}
	catch (const std::exception &e)
	{
		std::cerr << "Error was found: " << e.what() << '\n';
	}
	print_vector_specs(vector_default_1);
	vector_default_1.push_back(1);
	vector_default_1.push_back(2);
	vector_default_1.push_back(3);
	print_vector_specs(vector_default_1);
	std::cout << "Error 4" << std::endl;
	try
	{
		vector_default_1.pop_back();
	}
	catch (const std::length_error &e)
	{
		std::cout << e.what() << '\n';
	}
	print_vector_specs(vector_default_1);
	NAMESPACE::vector<TESTED_TYPE> vector_default_2(vector_default_1); // default
	print_vector_specs(vector_default_2);
	std::cout << "VECTOR ALLOCATORS" << std::endl;
	vector_default_1.clear();
	size_t len = 42;
	print_vector_specs(vector_default_1);
	TESTED_TYPE *new_data = vector_default_1.get_allocator().allocate(len);
	for (size_t i = 0; i < len; i++)
	{
		vector_default_1.get_allocator().construct(&new_data[i], i);
	}
	std::cout << "Array data allocated:";
	for (size_t i = 0; i < len; i++)
	{
		std::cout << i;
		if (i < len - 1)
			std::cout << ", ";
		else
			std::cout << std::endl;
	}
	for (size_t i = 0; i < len; i++)
	{
		vector_default_1.get_allocator().destroy(&new_data[i]);
	}
	vector_default_1.get_allocator().deallocate(new_data, len);
	std::cout << "NON MEMBERS" << std::endl;
	// Relational operators:
	std::vector<int> foo(3, 100); // three ints with a value of 100
	std::vector<int> bar(2, 200); // two ints with a value of 200
	if (foo == bar)
		std::cout << "foo and bar are equal" << std::endl;
	if (foo != bar)
		std::cout << "foo and bar are not equal" << std::endl;
	if (foo < bar)
		std::cout << "foo is less than bar" << std::endl;
	if (foo > bar)
		std::cout << "foo is greater than bar" << std::endl;
	if (foo <= bar)
		std::cout << "foo is less than or equal to bar" << std::endl;
	if (foo >= bar)
		std::cout << "foo is greater than or equal to bar" << std::endl;
	NAMESPACE::vector<TESTED_TYPE> vector_default_3(1, 11);
	NAMESPACE::vector<TESTED_TYPE> vector_default_4(2, 22);
	vector_default_3.swap(vector_default_4);
	print_vector_specs(vector_default_3, true);
	print_vector_specs(vector_default_4, true);
	vector_default_4.swap(vector_default_3);
	print_vector_specs(vector_default_3, true);
	print_vector_specs(vector_default_4, true);
	try
	{
		vector_default_3.swap(vector_default_3);
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << '\n';
	}
	print_vector_specs(vector_default_3, true);
	print_vector_specs(vector_default_4, true);
}

#endif //FT_CONTAINERS_TEST_VECTOR_OTHER_HPP

//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_VECTOR_CAPACITY_HPP
#define FT_CONTAINERS_TEST_VECTOR_CAPACITY_HPP

#include "test_vector_tools.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

using namespace NAMESPACE;

void test_vector_capacity()
{
	std::cout << std::endl << "TEST VECTOR CAPACITY" << std::endl;
	NAMESPACE::vector<TESTED_TYPE> vector_empty;
	NAMESPACE::vector<TESTED_TYPE> vector;
	for (int i = 0; i <= 42; i++)
	{
		vector.push_back(i);
	}
	print_vector_specs(vector_empty);
	print_vector_specs(vector);
	std::cout 	<< "Check if vector is empty: " << std::endl
				<< "Vector empty: " << vector_empty.empty() << std::endl
				<< "Vector filled: " << vector.empty() << std::endl;
	try
	{
		vector.reserve(80);
		print_vector_specs(vector);
		vector.reserve(12);
		print_vector_specs(vector);
		vector.reserve(1);
		print_vector_specs(vector);
		vector.reserve(0);
		print_vector_specs(vector);
		vector.reserve(-1);
		print_vector_specs(vector);
	}
	catch (const std::exception &e)
	{
		std::cerr << "error found with reserve: " << e.what() << std::endl;
	}
}

#endif //FT_CONTAINERS_TEST_VECTOR_CAPACITY_HPP

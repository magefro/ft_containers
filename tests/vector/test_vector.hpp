//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_VECTOR_HPP
#define FT_CONTAINERS_TEST_VECTOR_HPP

#include "test_vector_capacity.hpp"
#include "test_vector_constructors.hpp"
#include "test_vector_element_access.hpp"
#include "test_vector_iterators.hpp"
#include "test_vector_modifiers.hpp"
#include "test_vector_other.hpp"



void test_vector()
{
	test_vector_constructors();
	test_vector_capacity();
	test_vector_element_access();
	test_vector_iterators();
	test_vector_modifiers();
	test_vector_other();
}

#endif //FT_CONTAINERS_TEST_VECTOR_HPP

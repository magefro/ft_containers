//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_SET_CAPACITY_HPP
#define FT_CONTAINERS_TEST_SET_CAPACITY_HPP

#include "test_set_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

void test_set_capacity()
{
	std::cout << "SET CAPACITY" << std::endl;
	NAMESPACE::set<int> set_1;
	std::cout << set_1.empty() << std::endl;
	std::cout << set_1.size() << std::endl;
	std::cout << set_1.max_size() << std::endl;
	set_1.insert(1);
	std::cout << set_1.empty() << std::endl;
	std::cout << set_1.size() << std::endl;
	std::cout << set_1.max_size() << std::endl;
}

#endif //FT_CONTAINERS_TEST_SET_CAPACITY_HPP

//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_SET_OPERATIONS_HPP
#define FT_CONTAINERS_TEST_SET_OPERATIONS_HPP

#include "test_set_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

void test_set_operations()
{
	NAMESPACE::set<int> set_0;
	NAMESPACE::set<int> set_1;
	int range_size = 100;
	for (int i = 1; i <= range_size; i++)
		set_1.insert(i);
	std::cout << *(set_1.find(range_size / 2)) << std::endl;
	std::cout << *(set_1.find(range_size / 3)) << std::endl;
	std::cout << *(set_1.find(range_size / 4)) << std::endl;
	std::cout << *(set_1.find(range_size / 5)) << std::endl;
	std::cout << *(set_1.find(range_size / 6)) << std::endl;
	std::cout << *(set_1.find(range_size / 7)) << std::endl;
	if (set_1.find(range_size + 69) == set_1.end())
		std::cout << "OK: set.end() found - set_1.find(range_size + 69)" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	if (set_0.find(range_size / 2) == set_0.end())
		std::cout << "OK: set.end() found - set_0.find(range_size / 2)" << std::endl;
	else
		std::cout << "FAILED: " << *(set_0.find(range_size / 2)) << std::endl;
	std::cout << *(set_1.lower_bound(range_size / 2)) << std::endl;
	std::cout << *(set_1.upper_bound(range_size / 2)) << std::endl;
	std::cout << *(set_1.lower_bound(range_size / 3)) << std::endl;
	std::cout << *(set_1.upper_bound(range_size / 3)) << std::endl;
	std::cout << *(set_1.lower_bound(range_size / 4)) << std::endl;
	std::cout << *(set_1.upper_bound(range_size / 4)) << std::endl;
	std::cout << *(set_1.lower_bound(range_size / 5)) << std::endl;
	std::cout << *(set_1.upper_bound(range_size / 5)) << std::endl;
	std::cout << *(set_1.lower_bound(range_size / 6)) << std::endl;
	std::cout << *(set_1.upper_bound(range_size / 6)) << std::endl;
	std::cout << *(set_1.lower_bound(range_size / 7)) << std::endl;
	std::cout << *(set_1.upper_bound(range_size / 7)) << std::endl;
	if (set_1.lower_bound(range_size + 69) == set_1.end())
		std::cout << "OK: set.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	if (set_1.upper_bound(range_size + 69) == set_1.end())
		std::cout << "OK: set.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	if (set_0.lower_bound(range_size / 2) == set_0.end())
		std::cout << "OK: set.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	if (set_0.upper_bound(range_size / 2) == set_0.end())
		std::cout << "OK: set.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	NAMESPACE::pair<NAMESPACE::set<int>::iterator, NAMESPACE::set<int>::iterator> result;
	result = set_1.equal_range(range_size / 5);
	std::cout << *(result.first) << " " << *(result.second) << std::endl;
	result = set_1.equal_range(range_size + 69);
	if (result.first == set_1.end() && result.second == set_1.end())
		std::cout << "OK: set.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	result = set_0.equal_range(range_size / 5);
	if (result.first == set_0.end() && result.second == set_0.end())
		std::cout << "OK: set.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	std::cout << (set_1.count(range_size / 2)) << std::endl;
	std::cout << (set_1.count(range_size / 3)) << std::endl;
	std::cout << (set_1.count(range_size / 4)) << std::endl;
	std::cout << (set_1.count(range_size / 5)) << std::endl;
	std::cout << (set_1.count(range_size / 6)) << std::endl;
	std::cout << (set_1.count(range_size / 7)) << std::endl;
	std::cout << (set_1.count(range_size + 69)) << std::endl; // non existing
	std::cout << (set_0.count(range_size / 2)) << std::endl;  // empty
}

#endif //FT_CONTAINERS_TEST_SET_OPERATIONS_HPP

//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_SET_CONSTRUCTORS_HPP
#define FT_CONTAINERS_TEST_SET_CONSTRUCTORS_HPP

#include "test_set_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

void test_set_constructors()
{
	std::cout << "TEST SET CONSTRUCTORS" << std::endl;
	NAMESPACE::set<int> set_1;
	NAMESPACE::set<int> set_2(set_1);
	print_set(set_1);
	NAMESPACE::set<int> set_6(set_1.begin(), set_1.end());
	print_set(set_6);
	std::list<int> list;
	for (int i = 0; i <= 500; i++)
		list.push_back(i);
	NAMESPACE::set<int> set_3(list.begin(), list.end());
	print_set(set_3);
	list.clear();
	for (int i = 1; i <= 100; i++)
		list.push_back(i);
	NAMESPACE::set<int> set_4(++(list.begin()), --(list.end()));
	print_set(set_4);
	NAMESPACE::set<int> set;
	for (int i = 1; i <= 100; i++)
		set.insert(i);
}

#endif //FT_CONTAINERS_TEST_SET_CONSTRUCTORS_HPP

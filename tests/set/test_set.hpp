//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_SET_HPP
#define FT_CONTAINERS_TEST_SET_HPP

#include "test_set_capacity.hpp"
#include "test_set_constructors.hpp"
#include "test_set_iterators.hpp"
#include "test_set_observers.hpp"
#include "test_set_operations.hpp"
#include "test_set_modifiers.hpp"
#include "test_set_other.hpp"

void test_set()
{
	test_set_capacity();
	test_set_constructors();
	test_set_iterators();
	test_set_observers();
	test_set_modifiers();
	test_set_operations();
	test_set_other();
}

#endif //FT_CONTAINERS_TEST_SET_HPP

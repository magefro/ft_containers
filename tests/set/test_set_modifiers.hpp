//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_SET_MODIFIERS_HPP
#define FT_CONTAINERS_TEST_SET_MODIFIERS_HPP

#include "test_set_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

void test_set_modifiers()
{
	std::cout << "SET DELETE SIMPLE BEGINNING" << std::endl;
	std::list<int> list;
	// delete from begining
	for (int limit = 1; limit < 42; limit++)
	{
		std::cout << std::endl;
		std::cout << "set with: " << limit << std::endl;
		std::cout << std::endl;
		for (int i = 1; i <= limit; i++)
			list.push_back(i * 5);
		NAMESPACE::set<int> set_1(list.begin(), list.end());
		print_set(set_1);
		for (int i = 1; i <= limit; i++)
			set_1.erase(set_1.begin());
		print_set(set_1);
	}
	std::cout << "SET DELETE SIMPLE EMD" << std::endl;
	list.clear();
	for (int limit = 1; limit < 42; limit++)
	{
		std::cout << std::endl;
		std::cout << "set with: " << limit << std::endl;
		std::cout << std::endl;
		for (int i = 1; i <= limit; i++)
			list.push_back(i + 65);
		NAMESPACE::set<int> set_2(list.begin(), list.end());
		print_set(set_2);
		for (int i = 1; i <= limit; i++)
			set_2.erase(--(set_2.end()));
		print_set(set_2);
	}
	std::cout << "SET DELETE SIMPLE MIDDLE" << std::endl;
	list.clear();
	int range_size = 50;
	for (int i = 1; i <= range_size; i++)
		list.push_back(i + 65);
	NAMESPACE::set<int> set_3(list.begin(), list.end());
	NAMESPACE::set<int>::iterator ite = set_3.begin();
	for (int i = 0; i < range_size / 2; i++)
		ite++;
	std::cout << "#4" << std::endl;
	set_3.erase(ite++); // post incrementation so iterator stays valid :)
	print_set(set_3);
	std::cout << "#3" << std::endl;
	set_3.erase(ite++);
	print_set(set_3);
	std::cout << "#2" << std::endl;
	set_3.erase(ite++);
	print_set(set_3);
	std::cout << "#1" << std::endl;
	set_3.erase(ite);
	print_set(set_3);
	std::cout << "SET DELETE RAMGE" << std::endl;
	list.clear();
	range_size = 42;
	for (int i = 1; i <= range_size; i++)
		list.push_back(i);
	NAMESPACE::set<int> set_5(list.begin(), list.end());
	// delete a range
	std::cout << "#8" << std::endl;
	set_5.erase(--(set_5.end()), set_5.end());
	print_set(set_5);
	std::cout << "#7" << std::endl;
	set_5.erase(--(--(set_5.end())), set_5.end());
	print_set(set_5);
	std::cout << "#6" << std::endl;
	set_5.erase(--(--(set_5.end())), --(--(set_5.end())));
	print_set(set_5);
	std::cout << "#5" << std::endl;
	set_5.erase(set_5.begin(), ++(set_5.begin()));
	print_set(set_5);
	std::cout << "#4" << std::endl;
	set_5.erase(set_5.begin(), ++(++(set_5.begin())));
	print_set(set_5);
	std::cout << "#3" << std::endl;
	set_5.erase(set_5.begin(), ++(++(set_5.begin())));
	print_set(set_5);
	std::cout << "#2" << std::endl;
	set_5.erase(set_5.begin(), set_5.end()); // try to erase empty
	print_set(set_5);
	std::cout << "#1" << std::endl;
	set_5.erase(set_5.begin(), set_5.end()); // try to erase empty a second time
	print_set(set_5);
	std::cout << "SET INSERT" << std::endl;
	NAMESPACE::set<int> set_7;
	NAMESPACE::set<int> set_8;
	NAMESPACE::set<int> set_9;
	range_size = 100;
	for (int i = 1; i <= range_size; i++)
		set_7.insert(i);
	for (int i = 1; i <= range_size; i++)
		set_8.insert(i);
	for (int i = 1; i <= range_size / 2; i++)
		set_9.insert(i);
	set_7.swap(set_8);
	print_set(set_7);
	print_set(set_8);
	set_7.swap(set_8);
	print_set(set_7);
	print_set(set_8);
	set_8.swap(set_9);
	print_set(set_7);
	set_7.swap(set_9);
	print_set(set_8);
	// Clear
	print_set(set_7);
	set_7.clear();
	print_set(set_7);
	set_7.clear(); // clearing empty
	print_set(set_7);
}

#endif //FT_CONTAINERS_TEST_SET_MODIFIERS_HPP

//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_SET_TOOLS_HPP
#define FT_CONTAINERS_TEST_SET_TOOLS_HPP

#include <set>
#include <iostream>
#include <list>
#include "../../includes/set.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

using namespace NAMESPACE;

template <typename set_type>
void print_set(set_type const &set)
{
	std::cout << "size: " << set.size() << std::endl;
	std::cout << "max_size: " << set.max_size() << std::endl;

	typename set_type::const_iterator begin = set.begin();
	typename set_type::const_iterator end = set.end();
	while (begin != end)
	{
		std::cout << *begin << std::endl;
		begin++;
	}
}

#endif //FT_CONTAINERS_TEST_SET_TOOLS_HPP

//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_SET_ITERATORS_HPP
#define FT_CONTAINERS_TEST_SET_ITERATORS_HPP

#include "test_set_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

void test_set_iterators()
{
	std::cout << "SET ITERATOR" << std::endl;
	NAMESPACE::set<int> set_1;
	set_1.insert(NAMESPACE::set<int>::value_type(-4));
	set_1.insert(NAMESPACE::set<int>::value_type(1));
	set_1.insert(NAMESPACE::set<int>::value_type(3));
	set_1.insert(NAMESPACE::set<int>::value_type(69));

	std::cout << "begin " << *(set_1.begin()) << std::endl;
	std::cout << "rbegin" << *(set_1.rbegin()) << std::endl;
	// std::cout << "rend " << (set_1.rend())->first << std::endl;

	NAMESPACE::set<int>::iterator ite = set_1.begin();
	while (ite != set_1.end())
	{
		std::cout << *ite << std::endl;
		ite++;
	}

	std::cout << "--" << std::endl;

	NAMESPACE::set<int>::reverse_iterator r_ite = set_1.rbegin();
	while (r_ite != set_1.rend())
	{
		std::cout << *r_ite << std::endl;
		r_ite++;
	}

	std::cout << "--" << std::endl;
	std::cout << "--" << std::endl;
	NAMESPACE::set<int>::iterator ite_normal = ++set_1.begin();
	NAMESPACE::set<int>::reverse_iterator r_ite_normal(ite_normal);

	std::cout << "ite_normal " << *ite_normal << std::endl;
	std::cout << "ite_normal " << *r_ite_normal << std::endl;
}

#endif //FT_CONTAINERS_TEST_SET_ITERATORS_HPP

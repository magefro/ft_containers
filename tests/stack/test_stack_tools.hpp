//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_STACK_TOOLS_HPP
#define FT_CONTAINERS_TEST_STACK_TOOLS_HPP

#include <stack>
#include <deque>
#include "../../includes/stack.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

using namespace NAMESPACE;

template <typename T, typename U>
void print_stack_specs(NAMESPACE::stack<T, U> &stack)
{
	std::cout << "-----------------------------------------" << std::endl;
	std::cout
			<< "STACK SPECS" << std::endl
			<< "size: " << stack.size() << std::endl;

	std::cout << std::endl;
	std::cout << "-----------------------------------------" << std::endl;

}

#endif //FT_CONTAINERS_TEST_STACK_TOOLS_HPP

//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_STACK_HPP
#define FT_CONTAINERS_TEST_STACK_HPP

#include "test_stack_constructor.hpp"
#include "test_stack_functions.hpp"
#include "test_stack_operators.hpp"

void test_stack()
{
	test_stack_constructor();
	test_stack_functions();
	test_stack_operators();
}

#endif //FT_CONTAINERS_TEST_STACK_HPP

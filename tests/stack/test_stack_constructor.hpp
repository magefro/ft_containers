//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_STACK_CONSTRUCTOR_HPP
#define FT_CONTAINERS_TEST_STACK_CONSTRUCTOR_HPP

#include "test_stack_tools.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

void	test_stack_constructor()
{
	std::cout << "TEST STACK CONSTRUCTORS" << std::endl;
	NAMESPACE::stack<TESTED_TYPE> 									stack_1;
	NAMESPACE::stack<TESTED_TYPE, NAMESPACE::vector<TESTED_TYPE> > 	stack_2;
	NAMESPACE::stack<TESTED_TYPE, ft::vector<TESTED_TYPE> >			stack_3;
	NAMESPACE::stack<TESTED_TYPE, std::vector<TESTED_TYPE> >		stack_4;
	NAMESPACE::stack<TESTED_TYPE, std::deque<TESTED_TYPE> >			stack_5;
}

#endif //FT_CONTAINERS_TEST_STACK_CONSTRUCTOR_HPP

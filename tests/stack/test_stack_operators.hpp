//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_STACK_OPERATORS_HPP
#define FT_CONTAINERS_TEST_STACK_OPERATORS_HPP

#include "test_stack_tools.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

void test_stack_operators()
{
	std::cout << "TEST STACK OPERATORS" << std::endl;
	NAMESPACE::stack<TESTED_TYPE>											stack_1;
	stack_1.push(1);
	NAMESPACE::stack<TESTED_TYPE, NAMESPACE::vector<TESTED_TYPE> >	stack_2;
	stack_2.push(16);
	stack_2.push(16);
	NAMESPACE::stack<TESTED_TYPE, ft::vector<TESTED_TYPE> >					stack_3;
	stack_3.push(31);
	stack_3.push(31);
	NAMESPACE::stack<TESTED_TYPE, std::vector<TESTED_TYPE> >				stack_4;
	stack_4.push(46);
	stack_4.push(46);
	NAMESPACE::stack<TESTED_TYPE, std::deque<TESTED_TYPE> >					stack_5;
	stack_5.push(61);
	stack_5.push(61);
	stack_5.push(61);
}

#endif //FT_CONTAINERS_TEST_STACK_OPERATORS_HPP

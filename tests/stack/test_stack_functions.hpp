//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_STACK_FUNCTIONS_HPP
#define FT_CONTAINERS_TEST_STACK_FUNCTIONS_HPP

#include "test_stack_tools.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

void test_stack_functions()
{
	std::cout << "TEST STACK FUNCTIONS" << std::endl;
	NAMESPACE::stack<TESTED_TYPE>											stack_1;
	NAMESPACE::stack<TESTED_TYPE, NAMESPACE::vector<TESTED_TYPE> >			stack_2;
	NAMESPACE::stack<TESTED_TYPE, ft::vector<TESTED_TYPE> >					stack_3;
	NAMESPACE::stack<TESTED_TYPE, std::vector<TESTED_TYPE> >				stack_4;
	NAMESPACE::stack<TESTED_TYPE, std::deque<TESTED_TYPE> >					stack_5;

	std::cout << "Is empty? " << stack_5.empty() << std::endl;
	std::cout << "Is empty? " << stack_4.empty() << std::endl;
	std::cout << "Is empty? " << stack_3.empty() << std::endl;
	std::cout << "Is empty? " << stack_2.empty() << std::endl;
	std::cout << "Is empty? " << stack_1.empty() << std::endl;

	std::cout << "Size? " << stack_5.size() << std::endl;
	std::cout << "Size? " << stack_4.size() << std::endl;
	std::cout << "Size? " << stack_3.size() << std::endl;
	std::cout << "Size? " << stack_2.size() << std::endl;
	std::cout << "Size? " << stack_1.size() << std::endl;

	stack_1.push(1);
	std::cout << stack_1.top() << std::endl;
	stack_1.push(2);
	std::cout << stack_1.top() << std::endl;
	stack_1.push(3);
	std::cout << stack_1.top() << std::endl;
	stack_1.push(4);
	std::cout << stack_1.top() << std::endl;
	stack_1.push(5);
	std::cout << stack_1.top() << std::endl;
	stack_1.pop();
	std::cout << stack_1.top() << std::endl;
	stack_1.pop();
	std::cout << stack_1.top() << std::endl;
	stack_1.pop();
	std::cout << stack_1.top() << std::endl;
	stack_1.pop();
	std::cout << stack_1.top() << std::endl;

	stack_2.push(1);
	std::cout << stack_2.top() << std::endl;
	stack_2.push(2);
	std::cout << stack_2.top() << std::endl;
	stack_2.push(3);
	std::cout << stack_2.top() << std::endl;
	stack_2.push(4);
	std::cout << stack_2.top() << std::endl;
	stack_2.push(5);
	std::cout << stack_2.top() << std::endl;
	stack_2.pop();
	std::cout << stack_2.top() << std::endl;
	stack_2.pop();
	std::cout << stack_2.top() << std::endl;
	stack_2.pop();
	std::cout << stack_2.top() << std::endl;
	stack_2.pop();
	std::cout << stack_2.top() << std::endl;

	stack_3.push(1);
	std::cout << stack_3.top() << std::endl;
	stack_3.push(2);
	std::cout << stack_3.top() << std::endl;
	stack_3.push(3);
	std::cout << stack_3.top() << std::endl;
	stack_3.push(4);
	std::cout << stack_3.top() << std::endl;
	stack_3.push(5);
	std::cout << stack_3.top() << std::endl;
	stack_3.pop();
	std::cout << stack_3.top() << std::endl;
	stack_3.pop();
	std::cout << stack_3.top() << std::endl;
	stack_3.pop();
	std::cout << stack_3.top() << std::endl;
	stack_3.pop();
	std::cout << stack_3.top() << std::endl;

	stack_4.push(1);
	std::cout << stack_4.top() << std::endl;
	stack_4.push(2);
	std::cout << stack_4.top() << std::endl;
	stack_4.push(3);
	std::cout << stack_4.top() << std::endl;
	stack_4.push(4);
	std::cout << stack_4.top() << std::endl;
	stack_4.push(5);
	std::cout << stack_4.top() << std::endl;
	stack_4.pop();
	std::cout << stack_4.top() << std::endl;
	stack_4.pop();
	std::cout << stack_4.top() << std::endl;
	stack_4.pop();
	std::cout << stack_4.top() << std::endl;
	stack_4.pop();
	std::cout << stack_4.top() << std::endl;

	stack_5.push(1);
	std::cout << stack_5.top() << std::endl;
	stack_5.push(2);
	std::cout << stack_5.top() << std::endl;
	stack_5.push(3);
	std::cout << stack_5.top() << std::endl;
	stack_5.push(4);
	std::cout << stack_5.top() << std::endl;
	stack_5.push(5);
	std::cout << stack_5.top() << std::endl;
	stack_5.pop();
	std::cout << stack_5.top() << std::endl;
	stack_5.pop();
	std::cout << stack_5.top() << std::endl;
	stack_5.pop();
	std::cout << stack_5.top() << std::endl;
	stack_5.pop();
	std::cout << stack_5.top() << std::endl;
}

#endif //FT_CONTAINERS_TEST_STACK_FUNCTIONS_HPP

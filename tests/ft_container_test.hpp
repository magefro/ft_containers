//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_FT_CONTAINER_TEST_HPP
#define FT_CONTAINERS_FT_CONTAINER_TEST_HPP

#include "./vector/test_vector.hpp"
#include "./stack/test_stack.hpp"
#include "./map/test_map.hpp"
#include "./set/test_set.hpp"

#endif //FT_CONTAINERS_FT_CONTAINER_TEST_HPP

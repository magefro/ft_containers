//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_MAP_MODIFIERS_HPP
#define FT_CONTAINERS_TEST_MAP_MODIFIERS_HPP

#include "test_map_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

typedef NAMESPACE::pair<const T1, T2> T3;

void test_map_modifiers()
{
	std::cout << "MAP CONSTRUCTORS INSERT" << std::endl;
	NAMESPACE::map<int, int> map;
	int range_size = 100;
	for (int i = 1; i <= range_size; i++)
		map.insert(NAMESPACE::pair<int, int>(i, -i));
	std::cout << "MAP DELETE SIMPLE BEGINNING" << std::endl;
	std::list<T3> list;
	// delete from begining
	for (int limit = 1; limit < 42; limit++)
	{
		std::cout << std::endl;
		std::cout << "Map with: " << limit << std::endl;
		std::cout << std::endl;
		for (int i = 1; i <= limit; i++)
			list.push_back(T3(i, std::string(1, i + 65)));
		NAMESPACE::map<T1, T2> map_1(list.begin(), list.end());
		print_map(map_1);
		for (int i = 1; i <= limit; i++)
			map_1.erase(map_1.begin());
		print_map(map_1);
	}
	std::cout << "MAP DELETE SIMPLE EMD" << std::endl;
	list.clear();
	for (int limit = 1; limit < 42; limit++)
	{
		std::cout << std::endl;
		std::cout << "Map with: " << limit << std::endl;
		std::cout << std::endl;
		for (int i = 1; i <= limit; i++)
			list.push_back(T3(i, std::string(1, i + 65)));
		NAMESPACE::map<T1, T2> map_2(list.begin(), list.end());
		print_map(map_2);
		for (int i = 1; i <= limit; i++)
			map_2.erase(--(map_2.end()));
		print_map(map_2);
	}
	std::cout << "MAP DELETE SIMPLE MIDDLE" << std::endl;
	list.clear();
	range_size = 50;
	for (int i = 1; i <= range_size; i++)
		list.push_back(T3(i, std::string(1, i + 65)));
	NAMESPACE::map<T1, T2> map_3(list.begin(), list.end());
	NAMESPACE::map<T1, T2>::iterator ite = map_3.begin();
	for (int i = 0; i < range_size / 2; i++)
		ite++;
	std::cout << "#4" << std::endl;
	map_3.erase(ite++); // post incrementation so iterator stays valid :)
	print_map(map_3);
	std::cout << "#3" << std::endl;
	map_3.erase(ite++);
	print_map(map_3);
	std::cout << "#2" << std::endl;
	map_3.erase(ite++);
	print_map(map_3);
	std::cout << "#1" << std::endl;
	map_3.erase(ite);
	print_map(map_3);
	std::cout << "MAP DELETE RAMGE" << std::endl;
	list.clear();
	range_size = 42;
	for (int i = 1; i <= range_size; i++)
		list.push_back(T3(i, std::string(1, i + 65)));
	NAMESPACE::map<T1, T2> map_5(list.begin(), list.end());
	// delete a range
	std::cout << "#8" << std::endl;
	map_5.erase(--(map_5.end()), map_5.end());
	print_map(map_5);
	std::cout << "#7" << std::endl;
	map_5.erase(--(--(map_5.end())), map_5.end());
	print_map(map_5);
	std::cout << "#6" << std::endl;
	map_5.erase(--(--(map_5.end())), --(--(map_5.end())));
	print_map(map_5);
	std::cout << "#5" << std::endl;
	map_5.erase(map_5.begin(), ++(map_5.begin()));
	print_map(map_5);
	std::cout << "#4" << std::endl;
	map_5.erase(map_5.begin(), ++(++(map_5.begin())));
	print_map(map_5);
	std::cout << "#3" << std::endl;
	map_5.erase(map_5.begin(), ++(++(map_5.begin())));
	print_map(map_5);
	std::cout << "#2" << std::endl;
	map_5.erase(map_5.begin(), map_5.end()); // try to erase empty
	print_map(map_5);
	std::cout << "#1" << std::endl;
	map_5.erase(map_5.begin(), map_5.end()); // try to erase empty a second time
	print_map(map_5);
	std::cout << "MAP MODIFIERS" << std::endl;
	NAMESPACE::map<int, int> map_7;
	NAMESPACE::map<int, int> map_8;
	NAMESPACE::map<int, int> map_9;
	range_size = 100;
	for (int i = 1; i <= range_size; i++)
		map_7.insert(NAMESPACE::pair<int, int>(i, -i));
	for (int i = 1; i <= range_size; i++)
		map_8.insert(NAMESPACE::pair<int, int>(-i, i));
	for (int i = 1; i <= range_size / 2; i++)
		map_9.insert(NAMESPACE::pair<int, int>(-i, i));
	map_7.swap(map_8);
	print_map(map_7);
	print_map(map_8);
	map_7.swap(map_8);
	print_map(map_7);
	print_map(map_8);
	map_8.swap(map_9);
	print_map(map_7);
	map_7.swap(map_9);
	print_map(map_8);
	// Clear
	print_map(map_7);
	map_7.clear();
	print_map(map_7);
	map_7.clear(); // clearing empty
	print_map(map_7);
}

#endif //FT_CONTAINERS_TEST_MAP_MODIFIERS_HPP

//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_MAP_OTHER_HPP
#define FT_CONTAINERS_TEST_MAP_OTHER_HPP

#include "test_map_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

typedef NAMESPACE::pair<const T1, T2> T3;

void test_map_other()
{

}

#endif //FT_CONTAINERS_TEST_MAP_OTHER_HPP

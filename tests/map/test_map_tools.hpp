//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_MAP_TOOLS_HPP
#define FT_CONTAINERS_TEST_MAP_TOOLS_HPP

#include <map>
#include <list>
#include <iostream>
#include "../../includes/map.hpp"

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

using namespace NAMESPACE;

template <typename T>
void print_pair(const T &iterator)
{
	std::cout << "[" << iterator->first << "]"
			  << "[" << iterator->second << "]"
			  << std::endl;
}

template <typename map_type>
void print_map(map_type const &map)
{
	std::cout << "size: " << map.size() << std::endl;
	//std::cout << "max_size: " << map.max_size() << std::endl;

	typename map_type::const_iterator begin = map.begin();
	typename map_type::const_iterator end = map.end();
	while (begin != end)
	{
		print_pair(begin);
		begin++;
	}
}

#endif //FT_CONTAINERS_TEST_MAP_TOOLS_HPP

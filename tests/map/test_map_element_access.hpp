//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_MAP_ELEMENT_ACCESS_HPP
#define FT_CONTAINERS_TEST_MAP_ELEMENT_ACCESS_HPP

#include "test_map_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

typedef NAMESPACE::pair<const T1, T2> T3;

void test_map_element_access()
{
	std::cout << "MAP ELEMENT ACCESS" << std::endl;
	NAMESPACE::map<int, int> map;
	int range_size = 100;
	for (int i = 1; i <= range_size; i++)
		map.insert(NAMESPACE::pair<int, int>(i, -i));
	std::cout << map[range_size / 2] << std::endl;
	std::cout << map[range_size / 4] << std::endl;
	std::cout << map[range_size / 6] << std::endl;
	std::cout << map[range_size / 1] << std::endl;
	std::cout << map[range_size + 44] << std::endl; // with non existent value
}

#endif //FT_CONTAINERS_TEST_MAP_ELEMENT_ACCESS_HPP

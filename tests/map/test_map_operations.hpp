//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_MAP_OPERATIONS_HPP
#define FT_CONTAINERS_TEST_MAP_OPERATIONS_HPP

#include "test_map_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

typedef NAMESPACE::pair<const T1, T2> T3;

void test_map_operations()
{
	NAMESPACE::map<int, int> map_0;
	NAMESPACE::map<int, int> map_1;
	int range_size = 100;
	for (int i = 1; i <= range_size; i++)
		map_1.insert(NAMESPACE::pair<int, int>(i, -i));
	std::cout << (map_1.find(range_size / 2))->second << std::endl;
	std::cout << (map_1.find(range_size / 3))->second << std::endl;
	std::cout << (map_1.find(range_size / 4))->second << std::endl;
	std::cout << (map_1.find(range_size / 5))->second << std::endl;
	std::cout << (map_1.find(range_size / 6))->second << std::endl;
	std::cout << (map_1.find(range_size / 7))->second << std::endl;
	if (map_1.find(range_size + 69) == map_1.end())
		std::cout << "OK: map.end() found - map_1.find(range_size + 69)" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	if (map_0.find(range_size / 2) == map_0.end())
		std::cout << "OK: map.end() found - map_0.find(range_size / 2)" << std::endl;
	else
		std::cout << "FAILED: " << map_0.find(range_size / 2)->second << std::endl;
	std::cout << (map_1.lower_bound(range_size / 2))->second << std::endl;
	std::cout << (map_1.upper_bound(range_size / 2))->second << std::endl;
	std::cout << (map_1.lower_bound(range_size / 3))->second << std::endl;
	std::cout << (map_1.upper_bound(range_size / 3))->second << std::endl;
	std::cout << (map_1.lower_bound(range_size / 4))->second << std::endl;
	std::cout << (map_1.upper_bound(range_size / 4))->second << std::endl;
	std::cout << (map_1.lower_bound(range_size / 5))->second << std::endl;
	std::cout << (map_1.upper_bound(range_size / 5))->second << std::endl;
	std::cout << (map_1.lower_bound(range_size / 6))->second << std::endl;
	std::cout << (map_1.upper_bound(range_size / 6))->second << std::endl;
	std::cout << (map_1.lower_bound(range_size / 7))->second << std::endl;
	std::cout << (map_1.upper_bound(range_size / 7))->second << std::endl;
	if (map_1.lower_bound(range_size + 69) == map_1.end())
		std::cout << "OK: map.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	if (map_1.upper_bound(range_size + 69) == map_1.end())
		std::cout << "OK: map.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	if (map_0.lower_bound(range_size / 2) == map_0.end())
		std::cout << "OK: map.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	if (map_0.upper_bound(range_size / 2) == map_0.end())
		std::cout << "OK: map.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	NAMESPACE::pair<NAMESPACE::map<int, int>::iterator, NAMESPACE::map<int, int>::iterator> result;
	result = map_1.equal_range(range_size / 5);
	std::cout << (result.first)->second << " " << (result.second)->second << std::endl;
	result = map_1.equal_range(range_size + 69);
	if (result.first == map_1.end() && result.second == map_1.end())
		std::cout << "OK: map.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	result = map_0.equal_range(range_size / 5);
	if (result.first == map_0.end() && result.second == map_0.end())
		std::cout << "OK: map.end() found" << std::endl;
	else
		std::cout << "FAILED" << std::endl;
	std::cout << (map_1.count(range_size / 2)) << std::endl;
	std::cout << (map_1.count(range_size / 3)) << std::endl;
	std::cout << (map_1.count(range_size / 4)) << std::endl;
	std::cout << (map_1.count(range_size / 5)) << std::endl;
	std::cout << (map_1.count(range_size / 6)) << std::endl;
	std::cout << (map_1.count(range_size / 7)) << std::endl;
	std::cout << (map_1.count(range_size + 69)) << std::endl; // non existing
	std::cout << (map_0.count(range_size / 2)) << std::endl;  // empty
}

#endif //FT_CONTAINERS_TEST_MAP_OPERATIONS_HPP

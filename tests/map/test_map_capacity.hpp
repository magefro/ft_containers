//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_MAP_CAPACITY_HPP
#define FT_CONTAINERS_TEST_MAP_CAPACITY_HPP

#include "test_map_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

typedef NAMESPACE::pair<const T1, T2> T3;

void test_map_capacity()
{
	std::cout << "MAP CAPACITY" << std::endl;
	NAMESPACE::map<int, int> map_1;
	std::cout << map_1.empty() << std::endl;
	std::cout << map_1.size() << std::endl;
	//std::cout << map_1.max_size() << std::endl;
	map_1.insert(NAMESPACE::pair<int, int>(1, 1));
	std::cout << map_1.empty() << std::endl;
	std::cout << map_1.size() << std::endl;
	//std::cout << map_1.max_size() << std::endl;
}

#endif //FT_CONTAINERS_TEST_MAP_CAPACITY_HPP

//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_MAP_CONSTRUCTORS_HPP
#define FT_CONTAINERS_TEST_MAP_CONSTRUCTORS_HPP

#include "test_map_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

typedef NAMESPACE::pair<const T1, T2> T3;

void test_map_constructors()
{
	std::cout << "MAP CONSTRUCTORS BASIC" << std::endl;
	NAMESPACE::map<int, int> map_1;
	NAMESPACE::map<TESTED_TYPE, TESTED_TYPE> map_2(map_1);
	print_map(map_1);
	NAMESPACE::map<int, int> map_6(map_1.begin(), map_1.end());
	print_map(map_6);
	std::cout << "MAP CONSTRUCTORS RANGE 1" << std::endl;
	std::list<NAMESPACE::pair<int, int> > list;
	int range_size = 10;
	for (int i = 1; i <= range_size; i++)
		list.push_back(NAMESPACE::pair<int, int>(i, -i));
	NAMESPACE::map<int, int> map_3(list.begin(), list.end());
	print_map(map_3);
	std::cout << "MAP CONSTRUCTORS INSERT 1" << std::endl;
	std::list<NAMESPACE::pair<int, int> > list_1;
	range_size = 10;
	for (int i = 1; i <= range_size; i++)
		list_1.push_back(NAMESPACE::pair<int, int>(i, -i));
	NAMESPACE::map<int, int> map_4(list_1.begin(), list_1.end());
	print_map(map_4);
	std::cout << "MAP CONSTRUCTORS RANGE 2" << std::endl;
	std::list<NAMESPACE::pair<int, int> > list_2;
	range_size = 10;
	for (int i = 1; i <= range_size; i++)
		list_2.push_back(NAMESPACE::pair<int, int>(i, -i));
	std::cout << "insert 2" << std::endl;
	NAMESPACE::map<int, int> map_5(++(list_2.begin()), --(list_2.end()));
	std::cout << "insert 2: before print" << std::endl;
	print_map(map_5);
	std::cout << "insert 2: after print" << std::endl;
}

#endif //FT_CONTAINERS_TEST_MAP_CONSTRUCTORS_HPP

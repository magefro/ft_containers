//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_MAP_ITERATORS_HPP
#define FT_CONTAINERS_TEST_MAP_ITERATORS_HPP

#include "test_map_tools.hpp"

#define T1 int
#define T2 std::string

#ifndef NAMESPACE
#define NAMESPACE ft
#endif
#ifndef TESTED_TYPE
#define TESTED_TYPE int
#endif

typedef NAMESPACE::pair<const T1, T2> T3;

void test_map_iterators()
{
	std::cout << "MAP ITERATOR" << std::endl;
	NAMESPACE::map<int, int> map_1;
	map_1.insert(NAMESPACE::map<int, int>::value_type(-4, -4));
	map_1.insert(NAMESPACE::map<int, int>::value_type(1, 1));
	map_1.insert(NAMESPACE::map<int, int>::value_type(3, 3));
	map_1.insert(NAMESPACE::map<int, int>::value_type(69, 69));
	std::cout << "begin: " << (map_1.begin())->first << std::endl;
	std::cout << "rbegin: " << (map_1.rbegin())->first << std::endl;
	std::cout << "++begin: " << (++map_1.begin())->first << std::endl;
	std::cout << "++rbegin: " << (++map_1.rbegin())->first << std::endl;
	std::cout << "--end: " << (--map_1.end())->first << std::endl;
	std::cout << "--rend:" << (--map_1.rend())->first << std::endl;
	// std::cout << "rend " << (map_1.rend())->first << std::endl;
	NAMESPACE::map<int, int>::iterator ite = map_1.begin();
	while (ite != map_1.end())
	{
		std::cout << ite->first << std::endl;
		ite++;
	}
	std::cout << "--" << std::endl;
	NAMESPACE::map<int, int>::reverse_iterator r_ite = map_1.rbegin();
	while (r_ite != map_1.rend())
	{
		std::cout << r_ite->first << std::endl;
		r_ite++;
	}
	std::cout << "--" << std::endl;
	std::cout << "--" << std::endl;
	NAMESPACE::map<int, int>::iterator ite_normal = ++map_1.begin();
	NAMESPACE::map<int, int>::reverse_iterator r_ite_normal(ite_normal);
	std::cout << "ite_normal " << ite_normal->first << std::endl;
	std::cout << "ite_normal " << r_ite_normal->first << std::endl;
}

#endif //FT_CONTAINERS_TEST_MAP_ITERATORS_HPP

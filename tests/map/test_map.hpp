//
// Created by antoine on 15/06/22.
//

#ifndef FT_CONTAINERS_TEST_MAP_HPP
#define FT_CONTAINERS_TEST_MAP_HPP

#include "test_map_constructors.hpp"
#include "test_map_element_access.hpp"
#include "test_map_iterators.hpp"
#include "test_map_modifiers.hpp"
#include "test_map_observers.hpp"
#include "test_map_operations.hpp"
#include "test_map_other.hpp"
#include "test_map_capacity.hpp"

void test_map()
{
	test_map_constructors();
	test_map_element_access();
	test_map_iterators();
	test_map_modifiers();
	test_map_observers();
	test_map_operations();
	test_map_capacity();
	test_map_other();
}

#endif //FT_CONTAINERS_TEST_MAP_HPP

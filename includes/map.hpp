//
// Created by antoine on 03/03/2022.
//

#ifndef FT_CONTAINERS_MAP_HPP
#define FT_CONTAINERS_MAP_HPP

#include "lexicographical_compare.hpp"
#include "rbtree.hpp"
#include "equal.hpp"

/* Map
 * Maps are associative containers that store elements formed by a combination of a key value and a mapped value,
 * following a specific order.
 * In a map, the key values are generally used to sort and uniquely identify the elements, while the mapped values
 * store the content associated to this key. The types of key and mapped value may differ, and are grouped
 * together in member type value_type, which is a pair type combining both:
 *     typedef pair<const Key, T> value_type;
 * Internally, the elements in a map are always sorted by its key following a specific strict weak ordering criterion
 * indicated by its internal comparison object (of type Compare).
 * map containers are generally slower than unordered_map containers to access individual elements by their key,
 * but they allow the direct iteration on subsets based on their order.
 * The mapped values in a map can be accessed directly by their corresponding key useing the bracket operator[].
 * Maps are typically implemented as binary search trees.
 */

/* Container properties
 * Associative
 * Elements in associative containers are referenced by their key and not by their absolute position in the container.
 * Ordered
 * The elements in the container follow a strict order at all times. All inserted elements are given a position
 * in this order.
 * Map
 * Each element associates a key to a mapped value: Keys are meant to identify the elements whose main content is
 * the mapped value.
 * Unique keys
 * No two elements in the ontainer can have equivalent keys.
 * Allocator-aware
 * The container uses an allocator object to dynamically handle its storage needs.
 */

namespace ft {

	/* Template parameters
	 * Key
	 * Type of the keys. Each elements in a map is uniquely identified by its key value.
	 * Aliased as member type map::key_type.
	 * T
	 * Type of the mapped value. Each element in a map stores somes data as its mapped value.
	 * Aliased as member type map::mapped_type.
	 * Compare
	 * A binary predicate that takes two element keys as arguments and returns a bool. The expression comp(a,b),
	 * where comp is an object of this type and a and b are key values, shall return true if a is considered to go
	 * before b in the strict weak ordering the function defines.
	 * The map object uses this expression to determine both the order the elemts follow in the container and
	 * whether two element keys are equivalent (by comparing them reflexively: they are equivalent if !comp(a,b) &&
	 * !comp(b,a). No two elements in a map container can have equivalent keys.
	 * This can be a function pointer or a function object (see constructor for an example). This default to less<T>,
	 * which returns the same as applying the less-than operator (a<b).
	 * Aliased as member type map::key_compare.
	 * Alloc
	 * Type of the allocator object used to define the storage allocation model. By default, the allocator class
	 * template is used, which defines the simplest memory allocation model and is value-independent.
	 * Aliased as member type map::allocator_type.
	 */
	template <	class Key,                                    	    	//map::key_type
				class T,                                       			//map::mapped_type
				class Compare = std::less<Key>,                         //map::key_compare
				class Alloc = std::allocator<ft::pair<const Key, T> > >	//map::allocator_type
	class map
	{
			/******************************** MAP TYPEDEFS: **********************************/

		public:

			typedef 			Key											key_type;
			typedef				T											mapped_type;
			typedef             ft::pair<const key_type, mapped_type>		value_type;
			typedef 			Compare										key_compare;
			typedef				Alloc 										allocator_type;
			typedef typename	allocator_type::reference					reference;
			typedef typename	allocator_type::const_reference				const_reference;
			typedef typename	allocator_type::pointer						pointer;
			typedef typename	allocator_type::const_pointer				const_pointer;
			typedef 			std::ptrdiff_t								difference_type;
			typedef 			size_t										size_type;

			//enum 	rbtree_node_color { RED, BLACK };

			class				value_compare : public std::binary_function<value_type, value_type, bool> {

					friend class 											map;

				protected:

					Compare 												comp;
					value_compare (Compare c) : 							comp(c) {}

				public:

					typedef bool 											result_type;
					typedef value_type 										first_argument_type;
					typedef value_type 										second_argument_type;

					bool operator() (const value_type & x , const value_type & y) const {
						return (comp(x.first, y.first));
					}

			};

		protected:

			typedef ft::rbtree_node<value_type>								node_type;
			typedef ft::rbtree<value_type, node_type, value_compare, Alloc>	rbtree;

		public:

			typedef ft::rbtree_iterator<value_type, node_type>				iterator;
			typedef ft::rbtree_iterator<const value_type, node_type> 		const_iterator;
			typedef ft::reverse_iterator<iterator> 							reverse_iterator;
			typedef ft::reverse_iterator<const_iterator> 					const_reverse_iterator;

		/* Each node also stores its color, either red of black, using an enumeration. The role of the color bit will
		 * be explained in the properties. There is some internal fragmentation due to using an integer type to store
		 * a single bit, but we avoid optimizing this here for simplicity. In the source file we will typedef
		 * abbreviated names for our types.
		 */

			typedef typename 	Alloc::template rebind<node_type>::other	node_allocator_type;
			node_type *														root;

			/************************** MAP ATTRIBUTES: ****************************/

		private:

			allocator_type 													_alloc;
			value_compare													_comp;
			std::allocator<node_type>										_node_alloc;
			rbtree															_tree;

			/************************** MAP CONSTRUCTORS: ****************************/

		public:

			/* (1) empty container constructor (default constructor)
			 * Constructs an empty container, with no elements.
			 */
			map(const key_compare& comp = key_compare(), const allocator_type& alloc = allocator_type()) :
				_alloc(alloc),
				_comp(comp),
				_tree(comp, alloc)
			{
				return ;
			}

			/* (2) range constructor
			 * Constructs a container with as many elements as the range (first, last), with each element constructed from
			 * its coresponding element in that range.
			 */
			template <class InputIterator>
			map(InputIterator first, InputIterator last, const key_compare& comp = key_compare(), const allocator_type &alloc = allocator_type()) :
				_alloc(alloc),
				_comp(comp),
				_tree(comp, alloc)
			{
					this->insert(first, last);
			};

			/* (3) copy constructor
			 * Constructs a container with a copy of each of the elements in x.
			 */
			map(const map &x) :
				_alloc(x._alloc),
				_comp(x._comp),
				_tree(x._comp, x._alloc)
			{
				*this = x;
			};

			/* The container keeps an internal copy of alloc and comp, which are used to allocate storage and to sort the
			 * elements throughout its lifetime.
			 * The copy constructor (3) creates a container that keeps and uses copies of x's allocator and comparison object.
			 * The storage for the elements is allocated using this internal allocator.
			 */

			/* The elements are sorted according to the comparison object. If more than one element with equivalent keys is
			 * passed to the constructor, only the first one is preserved.
			 */


			/****************************** MAP DESTRUCTOR: ******************************/

			virtual ~map() {};

			/************************** MAP ASSIGNMENT OPERATOR: **************************/

			map &operator=(const map &src)
			{
				if (this != &src)
				{
					this->clear();
					this->insert(src.begin(), src.end());
				}
				return (*this);
			}

			/************************ MAP SUBSCRIPT OPERATOR ************************/

			mapped_type &operator[](const key_type& k)
			{
				iterator it = this->find(k);
				iterator last = this->end();

				if (it == last)
				{
					it = this->insert(ft::make_pair(k, mapped_type())).first;
				}
				return (it->second);
			}

			/**************************** MAP ITERATORS: ****************************/

			iterator begin()
			{
				return (this->_tree.begin());
			}

			const_iterator begin() const
			{
				return (this->_tree.begin());
			}

			iterator end()
			{
				return (this->_tree.end());
			}

			const_iterator end() const
			{
				return (this->_tree.end());
			}

			reverse_iterator rbegin()
			{
				return (this->_tree.rbegin());
			}

			const_reverse_iterator rbegin() const
			{
				return (this->_tree.rbegin());
			}

			reverse_iterator rend()
			{
				return (this->_tree.rend());
			}

			const_reverse_iterator rend() const
			{
				return (this->_tree.rend());
			}

			/**************************** MAP CAPACITY: ****************************/

			bool	empty() const
			{
				return (this->_tree.size == 0);
			}

			size_type	size() const
			{
				return (this->_tree.size);
			}

			size_type	max_size() const
			{
				return ((allocator_type().max_size() * sizeof(value_type)) / sizeof(node_type));
			}

			/***************************** MODIFIERS: *****************************/

			/* Insert elements
			 * Extends the container by inserting new elements, effectvely increasing the container size by
			 * the number of elements inserted.
			 * Because element keys in a map are unique, the insertion operation checks whether each inserted
			 * element has a key equivalent to the one of an element already in the container, and if so,
			 * the element is not inserted, returning an iterator to this existing element (if the function
			 * returns a value).
			 * For a similar container allowing for duplicate elements, see multimap.
			 * An alternative way to insert elements in a map is by using member function map::operator[]
			 * Internally, map containers keep all their elements sorted by their key following the criterion
			 * specified by its comparison object. The elements are always inserted in its respective position
			 * following this ordering.
			 * The parameters determine how many elements are inserted and to which values they are initialized.
			 */

			pair<iterator, bool> insert(const value_type& val)
			{
				return (this->_tree.rbtree_insert(this->end(), val));
			}

			iterator insert(iterator position, const value_type& val)
			{
				//using position as an hint
				return (this->_tree.rbtree_insert(position, val).first);
			}

			template <class InputIterator>
			void	insert(InputIterator first, InputIterator last)
			{
				while (first != last)
				{
					this->insert(this->end(), *first);
					first++;
				}

			}

			void	erase(iterator position)
			{
				this->_tree.rbtree_delete(*position);
			}

			size_type erase(const key_type& k)
			{
				size_t ret = this->count(k);
				if (ret > 0)
					this->_tree.rbtree_delete(this->find(k).node_ptr()->val);
				return (ret);
			}

			void	erase(iterator first, iterator last)
			{
				iterator it = first;
				while (first != last)
				{
					it++;
					this->_tree.rbtree_delete(*first);
					first = it;
				}
			}

			void	swap(map& x)
			{
				map tmp;
				tmp._copy_content_map(x);
				x._copy_content_map(*this);
				this->_copy_content_map(tmp);
			}

			void	clear()
			{
				this->_tree.rbtree_clear_all();
			}

			/**************************** MAP OPERATIONS: ****************************/


			iterator	find(const key_type &k)
			{
				return (this->_tree.rbtree_lookup(ft::make_pair(k, mapped_type())));

			}

			const_iterator	find(const key_type &k) const
			{
				return (this->_tree.rbtree_lookup(ft::make_pair(k, mapped_type())));
			}


			size_type	count(const key_type& k) const
			{
				return (this->_tree.rbtree_lookup(ft::make_pair(k, mapped_type())) != this->end());
			}

			iterator	lower_bound(const key_type& k)
			{
				return (this->_tree.lower_bound(ft::make_pair(k, mapped_type())));
			}

			const_iterator	lower_bound(const key_type& k) const
			{
				return (this->_tree.lower_bound(ft::make_pair(k, mapped_type())));
			}

			iterator	upper_bound(const key_type& k)
			{
				return (this->_tree.upper_bound(ft::make_pair(k, mapped_type())));
			}

			const_iterator	upper_bound(const key_type& k) const
			{
				return (this->_tree.upper_bound(ft::make_pair(k, mapped_type())));
			}

			pair<iterator, iterator>	equal_range(const key_type& k)
			{
				pair<iterator, iterator> ret;

				ret.first = this->lower_bound(k);
				ret.second = this->upper_bound(k);

				return (ret);
			}

			pair<const_iterator, const_iterator>	equal_range(const key_type& k) const
			{
				pair<const_iterator, const_iterator> ret;

				ret.first = this->lower_bound(k);
				ret.second = this->upper_bound(k);

				return (ret);
			}

			/**************************** MAP ALLOCATOR: ****************************/

			allocator_type	get_allocator() const
			{
				return (this->_alloc);
			}

			/**************************** MAP OBSERVERS: *****************************/

			key_compare	key_comp(void) const
			{
				return (key_compare());
			}

			value_compare value_comp() const
			{
				return (value_compare(key_compare()));
			}

			/************************* MAP PRIVATE FUNCTIONS: *************************/

		private:

			void _copy_content_map(map &x)
			{
				this->clear();

				node_type *tmp_root = this->_tree.root;
				node_type *tmp_ghost = this->_tree.ghost;

				this->_alloc = x._alloc;
				this->_comp = x._comp;
				this->_node_alloc = x._node_alloc;
				this->_tree.size = x._tree.size;
				this->_tree.root = x._tree.root;
				this->_tree.ghost = x._tree.ghost;

				x._tree.root = tmp_root;
				x._tree.ghost = tmp_ghost;
				x._tree.size = 0;
			}

	};

	/***************************** MAP NON MEMBER: ********************************/

	template <class Key, class T, class Compare, class Alloc>
	void swap(map<Key, T, Compare, Alloc> &x, map <Key, T, Compare, Alloc> &y)
	{
		x.swap(y);
	}

	template <class Key, class T, class Compare, class Alloc>
	bool operator==(const map<Key, T, Compare, Alloc> &lhs, const map<Key, T, Compare, Alloc> &rhs)
	{
		if (lhs.size() != rhs.size())
			return (false);
		return (ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
	}

	template <class Key, class T, class Compare, class Alloc>
	bool operator!=(const map<Key, T, Compare, Alloc> &lhs, const map<Key, T, Compare, Alloc> &rhs)
	{
		if (lhs == rhs)
			return (false);
		return (true);
	}

	template <class Key, class T, class Compare, class Alloc>
	bool operator<(const map<Key, T, Compare, Alloc> &lhs, const map<Key, T, Compare, Alloc> &rhs)
	{
		return (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()));
	}

	template <class Key, class T, class Compare, class Alloc>
	bool operator<=(const map<Key, T, Compare, Alloc> &lhs, const map<Key, T, Compare, Alloc> &rhs)
	{
		return !(rhs < lhs);
	}

	template <class Key, class T, class Compare, class Alloc>
	bool operator>(const map<Key, T, Compare, Alloc> &lhs, const map<Key, T, Compare, Alloc> &rhs)
	{
		return (rhs < lhs);
	}

	template <class Key, class T, class Compare, class Alloc>
	bool operator>=(const map<Key, T, Compare, Alloc> &lhs, const map<Key, T, Compare, Alloc> &rhs)
	{
		return !(lhs < rhs);
	}

}

#endif //FT_CONTAINERS_MAP_HPP

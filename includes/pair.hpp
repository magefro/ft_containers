//
// Created by alangloi on 5/3/22.
//

#ifndef FT_CONTAINERS_PAIR_HPP
#define FT_CONTAINERS_PAIR_HPP

namespace ft {

	template <class T1, class T2>
	struct pair
	{
			/*************************** PAIR ATTRIBUTES: ******************************/

		public:

			T1 first;
			T2 second;

			/*************************** PAIR CONSTRUCTORS: ******************************/

			pair() : first(), second() {};
			template<class T, class U>
			pair(const pair<T, U> &pr) : first(pr.first), second(pr.second) {};
			pair(const T1 &a, const T2 &b) : first(a), second(b) {};

			/****************************** PAIR OPERATOR: ********************************/

			operator pair<T1 const, T2 const>(void) const
			{
				return (pair<T1 const, T2 const>(this->first, this->second));
			}
	};

	/*************************** PAIR NON MEMBER: ******************************/

	template <class T1, class T2>
	bool operator==(const ft::pair<T1, T2>& lhs, const ft::pair<T1, T2>& rhs)
	{
		return (lhs.first == rhs.first && lhs.second == rhs.second);
	}

	template <class T1, class T2>
	bool operator!=(const ft::pair<T1, T2>& lhs, const ft::pair<T1, T2>& rhs)
	{
		return (!(lhs == rhs));
	}

	template <class T1, class T2>
	bool operator<(const ft::pair<T1, T2>& lhs, const ft::pair<T1, T2>& rhs)
	{
		return (lhs.first < rhs.first || (!(rhs.first < lhs.first) && lhs.second < rhs.second));
	}

	template <class T1, class T2>
	bool operator<=(const ft::pair<T1, T2>& lhs, const ft::pair<T1, T2>& rhs)
	{
		return (!(rhs < lhs));
	}

	template <class T1, class T2>
	bool operator>(const ft::pair<T1, T2>& lhs, const ft::pair<T1, T2>& rhs)
	{
		return (rhs < lhs);
	}

	template <class T1, class T2>
	bool operator >=(const ft::pair<T1, T2>& lhs, const ft::pair<T1, T2>& rhs)
	{
		return (!(lhs < rhs));
	}

}

#endif //FT_CONTAINERS_PAIR_HPP

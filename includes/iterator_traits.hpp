//
// Created by alangloi on 4/25/22.
//

#ifndef FT_CONTAINERS_ITERATOR_STRUCT_HPP
#define FT_CONTAINERS_ITERATOR_STRUCT_HPP

namespace ft {

/*
	struct input_iterator_tag {};
	struct output_iterator_tag {};
	struct forward_iterator_tag : public input_iterator_tag {};
	struct bidirectional_iterator_tag : public forward_iterator_tag {};
	struct random_access_iterator_tag : public bidirectional_iterator_tag {};
*/

	template<
			typename Category,
			typename T,
			typename Distance = std::ptrdiff_t,
			typename Pointer = T*,
			typename Reference = T&
	>
	struct iterator
	{
		typedef Category								iterator_category;
		typedef T										value_type;
		typedef Pointer									pointer;
		typedef Reference								reference;
		typedef Distance								difference_type;
	};



	template <typename Iterator>
	struct iterator_traits
	{
		typedef typename Iterator::difference_type		difference_type;
		typedef typename Iterator::reference			reference;
		typedef typename Iterator::value_type			value_type;
		typedef typename Iterator::pointer				pointer;
		typedef typename Iterator::iterator_category	iterator_category;
	};

	template <typename T>
	struct iterator_traits<T*>
	{
		typedef T										value_type;
		typedef std::ptrdiff_t							difference_type;
		typedef T*										pointer;
		typedef T&										reference;
		typedef std::random_access_iterator_tag			iterator_category;
	};

	template <typename T>
	struct iterator_traits<const T*>
	{
		typedef T										value_type;
		typedef std::ptrdiff_t							difference_type;
		typedef const T*								pointer;
		typedef const T&								reference;
		typedef std::random_access_iterator_tag			iterator_category;
	};

}

#endif //FT_CONTAINERS_ITERATOR_STRUCT_HPP

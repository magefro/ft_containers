//
// Created by alangloi on 5/3/22.
//

#ifndef FT_CONTAINERS_ENABLE_IF_HPP
#define FT_CONTAINERS_ENABLE_IF_HPP

namespace ft {

	/* The type T is enabled as member type enable_if::type if Cond is true.
	 * Otherwise, enable_if::type is not defined.
	 * This is usedful to hide signatures on compile time when a particualr condition
	 * is not met, since in this case, the member enable_if::type will not be defined
	 * and attempting to compile using it should fail. */
	template<bool Cond, class T = void>
	struct enable_if {};

	template<class T>
	struct enable_if<true, T>
	{
		typedef T type;
	};

}


#endif //FT_CONTAINERS_ENABLE_IF_HPP

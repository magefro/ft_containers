//
// Created by alangloi on 4/25/22.
//

#ifndef FT_CONTAINERS_VECTOR_ITERATOR_HPP
#define FT_CONTAINERS_VECTOR_ITERATOR_HPP

#include "iterator_traits.hpp"

namespace ft {

	template <typename T, bool isConst = false>
	class vector_iterator
	{
			/************************* VECTOR ITERATOR TYPEDEFS: **************************/

		private:

			typedef typename ft::iterator_traits<T*>	_traits;

		public:

			typedef typename _traits::value_type		value_type;
			typedef typename _traits::pointer			pointer;
			typedef typename _traits::reference			reference;
			typedef typename _traits::difference_type	difference_type;
			typedef typename _traits::iterator_category	iterator_category;

			/*************************** VECTOR ITERATOR ATTRIBUTES: ***************************/

			T	*_ptr;

		public:

			/************************* VECTOR ITERATOR CONSTRUCTORS: *************************/

			vector_iterator(): _ptr(NULL) {};
			vector_iterator(T* ptr) : _ptr(ptr) {};
			vector_iterator(const vector_iterator &it) : _ptr(it._ptr) {};

			/************************* VECTOR ITERATOR DESTRUCTORS: **************************/

			virtual ~vector_iterator() {};

			/*********************** VECTOR ITERATOR CONVERSION OPERATOR *******************/

			operator	vector_iterator<const T, true>() const
			{
				return (vector_iterator<const T, true>(_ptr));
			}

			/********************* VECTOR ITERATOR ASSIGNMENT OPERATOR: *********************/

			vector_iterator &operator=(const vector_iterator &it)
			{
				if (this != &it)
					this->_ptr = it._ptr;
				return (*this);
			}

			/******************** VECTOR ITERATOR ARITMETIC OPERATORS: **********************/

			vector_iterator &operator+=(difference_type val)
					{
				this->_ptr += val;
				return (*this);
			}

			vector_iterator &operator-=(difference_type val)
					{
				this->_ptr -= val;
				return (*this);
			}

			vector_iterator	operator+(difference_type val) const
			{
				vector_iterator tmp(*this);
				return (tmp += val);
			}

			vector_iterator operator-(difference_type val) const
			{
				vector_iterator tmp(*this);
				return (tmp -= val);
			}

			difference_type	operator-(vector_iterator<T, false> const &it) const
			{
				return (this->_ptr - it._ptr);
			}

			difference_type	operator-(vector_iterator<const T, true> const &it) const
			{
				return (this->_ptr - it._ptr);
			}

			/**************** VECTOR ITERATOR INCREMENT/DECREMENT OPERATORS: *****************/

		 	vector_iterator	&operator++(void)
			 { //pre-increment
				this->_ptr++;
				return (*this);
			}

			vector_iterator	operator++(int)
			{ // post-increment
				vector_iterator tmp(*this);
				this->_ptr++;
				return (tmp);
			 }

			vector_iterator	&operator--(void)
			{ //pre-decrement
				this->_ptr--;
				return (*this);
			}

			vector_iterator	operator--(int)
			{ // post-decrement
				vector_iterator tmp(*this);
				this->_ptr--;
				return (tmp);
			}

			/************************ VECTOR ITERATOR ACCESSORS: *************************/

			reference	operator*() const
			{
				return (*this->_ptr);
			}

			pointer 	operator->() const
			{
				return (this->_ptr);
			}

			reference 	operator[](difference_type it) const
			{
				return	(this->_ptr[it]);
			}

			/********************* VECTOR ITERATOR RELATIONNAL OPERATORS: **********************/

			bool	operator==(const vector_iterator<T, false> &it) const
			{
				return (this->_ptr == it._ptr);
			}

			bool	operator!=(const vector_iterator<T, false> &it) const
			{
				return (this->_ptr != it._ptr);
			}

			bool	operator<(const vector_iterator<T, false> &it) const
			{
				return (this->_ptr < it._ptr);
			}

			bool	operator>(const vector_iterator<T, false> &it) const
			{
				return (this->_ptr > it._ptr);
			}

			bool	operator<=(const vector_iterator<T, false> &it) const
			{
				return (this->_ptr <= it._ptr);
			}

			bool	operator>=(const vector_iterator<T, false> &it) const
			{
				return (this->_ptr >= it._ptr);
			}

			bool	operator==(const vector_iterator<const T, true> &it) const
			{
				return (this->_ptr == it._ptr);
			}

			bool	operator!=(const vector_iterator<const T, true> &it) const
			{
				return (this->_ptr != it._ptr);
			}

			bool	operator<(const vector_iterator<const T, true> &it) const
			{
				return (this->_ptr < it._ptr);
			}

			bool	operator>(const vector_iterator<const T, true> &it) const
			{
				return (this->_ptr > it._ptr);
			}

			bool	operator<=(const vector_iterator<const T, true> &it) const
			{
				return (this->_ptr <= it._ptr);
			}

			bool	operator>=(const vector_iterator<const T, true> &it) const
			{
				return (this->_ptr >= it._ptr);
			}
	};

	/*************************** VECTOR ITERATOR NON MEMBER: ****************************/

	template <typename It, bool isConst>
	vector_iterator<It, isConst> operator+(typename vector_iterator<It, isConst>::difference_type lhs, const vector_iterator<It, isConst>& rhs)
	{
		return (vector_iterator<It, isConst>(rhs + lhs));
	}
}

#endif //FT_CONTAINERS_VECTOR_ITERATOR_HPP

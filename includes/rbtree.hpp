//
// Created by alangloi on 5/24/22.
//

#ifndef FT_CONTAINERS_RBTREE
#define FT_CONTAINERS_RBTREE

#include <memory>
//#include <assert.h>
#include "rbtree_iterator.hpp"
#include "reverse_iterator.hpp"
#include "make_pair.hpp"

namespace ft {

	/******************************** ENUM COLOR: **********************************/

	enum rbtree_node_color {
		RED, BLACK
	};

	/******************************** RBTREE NODE CLASS: **********************************/

	template <class T>
	class rbtree_node {

			/***************************** RBTREE NODE ATTRIBUTES: *****************************/

		public:

			rbtree_node_color				color;
			T								val;
			rbtree_node						*left;
			rbtree_node						*right;
			rbtree_node						*parent;

			/***************************** RBTREE NODE CONSTRUCTOR: *****************************/

			rbtree_node(const T & src = T()) :
				color(BLACK),
				val(src),
				left(NULL),
				right(NULL),
				parent(NULL)
			{};

			/***************************** RBTREE NODE DESTRUCTOR: *****************************/

			virtual ~rbtree_node() {};

			/************************* RBTREE NODE ASSIGNMENT OPERATOR: *************************/

			rbtree_node &operator=(rbtree_node const &src)
			{
				if (this != &src)
				{
					this->val = src.val;
					this->color = src.color;
					this->left = src.left;
					this->right = src.right;
					this->parent = src.parent;
				}
				return (*this);
			}
	};

	/*********************************** RBTREE CLASS: **************************************/

	template <class value_type, class node_type, class value_compare, typename Alloc = std::allocator<value_type> >
	class rbtree
	{
			/******************************** RBTREE TYPEDEF: ***********************************/

		public:

			typedef Alloc 													allocator_type;
			typedef typename Alloc::template rebind<node_type>::other		node_allocator_type;
			typedef std::size_t 											size_type;
			typedef rbtree_node<value_type> 								node;
			typedef enum rbtree_node_color 									color;
			typedef ft::rbtree_iterator<value_type, node_type> 				iterator;
			typedef ft::rbtree_iterator<const value_type, node_type>		const_iterator;
			typedef ft::reverse_iterator<iterator> 							reverse_iterator;
			typedef ft::reverse_iterator<const_iterator> 					const_reverse_iterator;
			typedef std::ptrdiff_t											difference_type;
			typedef value_type*												pointer;

			/****************************** RBTREE ATTRIBUTES: *********************************/

			node_type														*root;
			node_type 														*ghost;
			size_t 															size;

		private:

			value_compare 													_comp;
			allocator_type 													_alloc;
			node_allocator_type 											_node_alloc;

		public:

			/****************************** RBTREE CONSTRUCTORS: *********************************/

			rbtree(const value_compare &comp = value_compare(), const Alloc &alloc = Alloc()) :
					root(NULL),
					size(0),
					_comp(comp),
					_alloc(alloc)
			{
				this->ghost = this->_node_alloc.allocate(1);
				this->_node_alloc.construct(this->ghost, node());
				this->ghost->color = BLACK;
				this->ghost->parent = this->ghost;
				this->ghost->left = this->ghost;
				this->ghost->right = this->ghost;
				this->root = this->ghost;
			}

			rbtree(rbtree const &src) :
				_comp(src._comp),
				_alloc(src._alloc),
				size(size_type(0))
			{
				this->ghost = this->_node_alloc.allocate(1);
				this->_node_alloc.construct(this->ghost, node());
				this->ghost->right = this->ghost;
				this->ghost->left = this->ghost;
				this->ghost->parent = this->ghost;
				this->ghost->color = BLACK;
				this->root = this->ghost;
				*this = src;
			}

			/****************************** RBTREE DESTRUCTOR: *********************************/

			virtual ~rbtree(void)
			{
				this->rbtree_clear(this->root);
				this->node_delete(this->ghost);
			}

			/************************** RBTREE ASSIGNMENT OPERATOR: *****************************/

			rbtree &operator=(rbtree const &src)
			{
				if (this != &src) {
					if (this->ghost->parent) {
						this->ghost->parent->right = this->ghost;
					}
					this->rbtree_clear(this->root);
					this->root = this->ghost;
					iterator it(src.begin());
					iterator last(src.end());
					while (it != last) {
						rbtree_insert(*it);
						++it;
					}
				}
			}


		public:

			/******************************** RBTREE ITERATOR *******************************/

			iterator begin(void)
			{
				return (iterator(this->min_node(this->root), this->root, this->ghost));
			}

			const_iterator begin(void) const
			{
				return (const_iterator(this->min_node(this->root), this->root, this->ghost));
			}

			iterator end(void)
			{
				return (iterator(this->ghost, this->root, this->ghost));
			}

			const_iterator end(void) const
			{
				return (const_iterator(this->ghost, this->root, this->ghost));
			}

			reverse_iterator rbegin(void)
			{
				return (reverse_iterator(this->end()));
			}

			const_reverse_iterator rbegin(void) const
			{
				return (const_reverse_iterator(this->end()));
			}

			reverse_iterator rend(void)
			{
				return (reverse_iterator(this->begin()));
			}

			const_reverse_iterator rend(void) const
			{
				return (const_reverse_iterator(this->begin()));
			}

			/*************************** RBTREE PROPERTIES: ***************************/

			node_type *_getGrandParent(node_type *n)
			{
				//assert(n != this->ghost);
				//assert(n->parent != this->ghost);        /* Not the root node */
				//assert(n->parent->parent != this->ghost); /* Not child of root */
				return (n->parent->parent);
			}

			node_type *_getSibling(node_type *n)
			{
				//assert(n != this->ghost);
				//assert(n->parent != this->ghost); /* Root node has no _getSibling */
				if (n == n->parent->left)
					return (n->parent->right);
				else
					return (n->parent->left);
			}

			node_type *_getUncle(node_type *n)
			{
				//assert(n != this->ghost);
				//assert(n->parent != this->ghost); /* Root node has no _getUncle */
				//assert(n->parent->parent != this->ghost); /* Children of root have no _getUncle */
				return (_getSibling(n->parent));
			}

			//All leaves (NULL) are black and contain no val. Since we represent these empty leaves using NULL, this
			//property is implicity assured by always treating NULL as black. To this end we create a node_color()
			//helper function.
			color node_color(node_type *n)
			{
				return (n == this->ghost ? BLACK : n->color);
			}

			/************************** RBTREE CONSTRUCTION: ****************************/

			node_type *new_node(value_type val, color node_color, node_type *left, node_type *right, node_type *parent)
			{
				node_type *result = this->_node_alloc.allocate(1);
				this->_node_alloc.construct(result, val);
				result->parent = parent;
				result->right = right;
				result->left = left;
				result->color = node_color;
				return (result);
			}

			/***************************** RBTREE SEARCH: ******************************/

			node_type *lookup_node(value_type const &val) const
			{
				node_type *n = this->root;
				while (n != this->ghost)
				{
					if (_comp(val, n->val))
					{
						n = n->left;
					}
					else if (_comp(n->val, val))
					{
						n = n->right;
					}
					else
					{
						break;
					}
				}
				return (n);
			}

			iterator rbtree_lookup(value_type const &val)
			{
				node_type *n = this->lookup_node(val);
				return (iterator(n, this->root, this->ghost));
			}

			const_iterator rbtree_lookup(value_type const &val) const
			{
				node_type *n = this->lookup_node(val);
				return (const_iterator(n, this->root, this->ghost));
			}

			/******************************* RBTREE ROTATE: *******************************/

			void replace_node(node_type *oldn, node_type *newn)
			{
				if (oldn->parent == this->ghost)
				{
					this->root = newn;
				} else
				{
					if (oldn == oldn->parent->left)
						oldn->parent->left = newn;
					else
						oldn->parent->right = newn;
				}
				if (newn != this->ghost)
				{
					newn->parent = oldn->parent;
				}
			}

			void rotate_left(node_type *n)
			{
				node_type *r = n->right;
				this->replace_node(n, r);
				n->right = r->left;
				if (r->left != this->ghost)
				{
					r->left->parent = n;
				}
				r->left = n;
				n->parent = r;
			}

			void rotate_right(node_type *n)
			{
				node_type *L = n->left;
				this->replace_node(n, L);
				n->left = L->right;
				if (L->right != this->ghost)
				{
					L->right->parent = n;
				}
				L->right = n;
				n->parent = L;
			}

			/****************************** RBTREE INSERT: *******************************/

			//In this final case, we deal with two cases that are mirror images of one another:
			//- The new node is the left child of its parent and the parent is the left child of the grandparent. In this case
			//we rotate right about the _getGrandParent.
			//- The new node is the right child of its parent and the parent is the right child of the grandparent. In this case
			//we rotate left about the _getGrandParent.
			void insert_case5(node_type *n)
			{
				n->parent->color = BLACK;
				_getGrandParent(n)->color = RED;
				if (n == n->parent->left && n->parent == _getGrandParent(n)->left)
				{
					this->rotate_right(_getGrandParent(n));
				}
				else
				{
					//assert(n == n->parent->right && n->parent == _getGrandParent(n)->right);
					this->rotate_left(_getGrandParent(n));
				}
			}

			//In this case, we deal with two cases that are mirror images of one another:
			//- The new node is the right child of its parent and the parent is the left child of the grandparent. In this
			//case we rotate left about the parent.
			//- The new node is the left child of its parent and the parent is the right child of the grandparent. In this
			//case we rotate right about the parent.
			void insert_case4(node_type *n)
			{
				if (n == n->parent->right && n->parent == _getGrandParent(n)->left)
				{
					this->rotate_left(n->parent);
					n = n->left;
				}
				else if (n == n->parent->left && n->parent == _getGrandParent(n)->right)
				{
					this->rotate_right(n->parent);
					n = n->right;
				}
				this->insert_case5(n);
			}

			//In this case, the _getUncle node is red. We recolor the parent and _getUncle black and the grandparent red. However, the
			//red grandparent node may now violate the red-black tree properties; we recursively invoke this procedure on it
			//from case 1 to deal with this.
			void insert_case3(node_type *n)
			{
				if (node_color(_getUncle(n)) == RED)
				{
					n->parent->color = BLACK;
					_getUncle(n)->color = BLACK;
					_getGrandParent(n)->color = RED;
					this->insert_case1(_getGrandParent(n));
				}
				else
				{
					this->insert_case4(n);
				}
			}

			//In this case the new node has a black parent. All the properties are still satisfied
			//and we return.
			void insert_case2(node_type *n)
			{
				if (node_color(n->parent) == BLACK)
					return ; //Tree is still valid
				else
					this->insert_case3(n);
			}

			//The call to insert_case1 above begins the process of correcting the tree so that it satisfies the properties once
			//more.
			void insert_case1(node_type *n)
			{
				if (n->parent == this->ghost)
					n->color = BLACK;
				else
					this->insert_case2(n);
			}

			//When inserting a new value, we first insert it into the tree as we would into an ordinary binary search tree.
			//If the key already exists, we just replace the value (since we're implementing an associative array). Otherwise,
			//we find the place in the tree where the new pair belong, then attach a newly created red node containing the value.
			ft::pair<iterator, bool> rbtree_insert(iterator hint, const value_type &val)
			{
				node_type *inserted_node;
				if (hint.ptr == this->ghost || !(this->_comp(val, hint.ptr->parent->val) == true
					&& this->_comp(hint.ptr->parent->val, val) == true)
					|| !(this->_comp(hint.ptr->left->val, val) == true
					&& this->_comp(val, hint.ptr->parent->val) == true))
					inserted_node = this->root;
				inserted_node = new_node(val, RED, this->ghost, this->ghost, this->ghost);
				if (this->root == this->ghost)
				{
					this->root = inserted_node;
				}
				else
				{
					node_type *n = this->root;
					while (1)
					{
						if (this->_comp(val, n->val))
						{
							if (n->left == this->ghost)
							{
								n->left = inserted_node;
								break;
							}
							else
							{
								n = n->left;
							}
						}
						else if (this->_comp(n->val, val))
						{
							if (n->right == this->ghost)
							{
								n->right = inserted_node;
								break;
							}
							else
							{
								n = n->right;
							}
						}
						else
						{
							this->node_delete(inserted_node);
							return (ft::make_pair(iterator(n, this->root, this->ghost), false));
						}
					}
					inserted_node->parent = n;
				}
				this->insert_case1(inserted_node);
				this->size++;
				return (ft::make_pair(iterator(inserted_node, this->root, this->ghost), true));
			}

			/****************************** RBTREE DELETE: *******************************/

			//There are two cases handled here which are mirror images of one another:
			//- N's _getSibling is black, S's right child is red, and N is the left child of its parent. We exchange the colors of
			//N's parent and _getSibling, make S's right child black, then rotate left at N's parent.
			//- N's _getSibling is black, S's left child is red, and N is the right child of its parent. We exchange the colors of
			//N's parent and _getSibling, make S's left child black, then rotate right at N's parent.
			//This accomplishes three things at once:
			//- We add a black node to all paths through N, either by adding a black S to those paths by recoloring N's parent
			// black.
			//- We remove a black node to all paths through S's red child, either by removing P from those paths or by
			// recoloring S.
			//- We recolor S's red child black, adding a black node back to all paths through S's red child.
			//S's left child has become a child of N's parent during the rotation and so is unaffected.
			void delete_case6(node_type *n)
			{
				_getSibling(n)->color = node_color(n->parent);
				n->parent->color = BLACK;
				if (n == n->parent->left)
				{
					//assert(node_color(_getSibling(n)->right) == RED);
					_getSibling(n)->right->color = BLACK;
					rotate_left(n->parent);
				}
				else
				{
					//assert(node_color(_getSibling(n)->left) == RED);
					_getSibling(n)->left->color = BLACK;
					rotate_right(n->parent);
				}
			}

			//There are two cases handled here which are mirror images of one another:
			//- N's _getSibling S is black, S's left child is red, S's right child is black, and N is the left child of its parent.
			//We exchange the colors of S and its left _getSibling and rotate right at S.
			//- N's _getSibling S is black, S's right child is red, S's left child is black. and N is the right child of its parent.
			//We exchange the colors of S and its right _getSibling and rotate left at S.
			void delete_case5(node_type *n)
			{
				if (n == n->parent->left &&
					node_color(_getSibling(n)) == BLACK &&
					node_color(_getSibling(n)->left) == RED &&
					node_color(_getSibling(n)->right) == BLACK)
				{
					_getSibling(n)->color = RED;
					_getSibling(n)->left->color = BLACK;
					rotate_right(_getSibling(n));
				}
				else if (n == n->parent->right &&
						   node_color(_getSibling(n)) == BLACK &&
						   node_color(_getSibling(n)->right) == RED &&
						   node_color(_getSibling(n)->left) == BLACK)
				{
					_getSibling(n)->color = RED;
					_getSibling(n)->right->color = BLACK;
					rotate_left(_getSibling(n));
				}
				delete_case6(n);
			}

			//N's _getSibling and _getSibling 's children are black, but its parent is red. We exchange the olors of the _getSibling and
			//parent, this restores the tree properties.
			void delete_case4(node_type *n)
			{
				if (node_color(n->parent) == RED &&
					node_color(_getSibling(n)) == BLACK &&
					node_color(_getSibling(n)->left) == BLACK &&
					node_color(_getSibling(n)->right) == BLACK)
				{
					_getSibling(n)->color = RED;
					n->parent->color = BLACK;
				}
				else
					delete_case5(n);
			}

			//In this case N's parent. _getSibling and _getSibling's children are black. In the case we paint the _getSibling red. Now all
			//paths passing through N's parent have one less black node than before the deletion, so we must recursively
			//run this procedure from case 1 on N's parent.
			void delete_case3(node_type *n)
			{
				if (node_color(n->parent) == BLACK &&
					node_color(_getSibling(n)) == BLACK &&
					node_color(_getSibling(n)->left) == BLACK &&
					node_color(_getSibling(n)->right) == BLACK)
				{
					_getSibling(n)->color = RED;
					delete_case1(n->parent);
				}
				else
					delete_case4(n);
			}

			//N has a red _getSibling. In this case we exchange the colors of the parent and _getSibling, then rotate about the parent
			//so that the _getSibling becomes the parent of its former parent. This does not restore the tree properties, but
			//reduces the problem to one of the remaining cases.
			void delete_case2(node_type *n)
			{
				if (node_color(_getSibling(n)) == RED)
				{
					n->parent->color = RED;
					_getSibling(n)->color = BLACK;
					if (n == n->parent->left)
						rotate_left(n->parent);
					else
						rotate_right(n->parent);
				}
				delete_case3(n);
			}

			//In this case, N has become the root node. The deletion remord one black node from every path, so no properties
			//are violated
			void delete_case1(node_type *n)
			{
				if (n->parent == this->ghost)
					return;
				else
					delete_case2(n);
			}

			void swap_node(node_type *n1, node_type *n2)
			{
				node_type	*parent_tmp = n1->parent;
				node_type	*left_tmp = n1->left;
				node_type	*right_tmp = n1->right;
				color		color_tmp = n1->color;
				if (n1->parent != this->ghost && n1->parent->left == n1)
					n1->parent->left = n2;
				else if (n1->parent != this->ghost)
					n1->parent->right = n2;
				else
					this->root = n2;
				if (n2->parent != this->ghost && n2->parent->left == n2)
					n2->parent->left = n1;
				else if (n2->parent != this->ghost)
					n2->parent->right = n1;
				else
					this->root = n1;
				n1->parent = (n2->parent == n1) ? n2 : n2->parent;
				n1->left = n2->left;
				n1->right = n2->right;
				if (n1->right != this->ghost)
					n1->right->parent = n1;
				if (n1->left != this->ghost)
					n1->left->parent = n1;
				n1->color = n2->color;
				n2->parent = parent_tmp;
				n2->left = (left_tmp == n2) ? n1 : left_tmp;
				n2->right = (right_tmp == n2) ? n1 : right_tmp;
				if (n2->right != this->ghost)
					n2->right->parent = n2;
				if (n2->left != this->ghost)
					n2->left->parent = n2;
				n2->color = color_tmp;
			}

			void rbtree_delete(value_type val)
			{
				node_type	*child;
				node_type	*n = lookup_node(val);
				this->size--;
				if (this->size == 0)
				{
					this->rbtree_clear_all();
					return ;
				}
				if (n->left != this->ghost && n->right != this->ghost)
				{
					node_type *pred = this->max_node(n->left);
					this->swap_node(n, pred);
				}
				//assert(n->left == this->ghost || n->right == this->ghost );
				child = n->right == this->ghost ? n->left : n->right;
				if (node_color(n) == BLACK)
				{
					n->color = node_color(child);
					delete_case1(n);
				}
				replace_node(n, child);
				if (n->parent == this->ghost && child != this->ghost) //root should be black
					child->color = BLACK;
				if (n != this->ghost)
				{
					this->_node_alloc.destroy(n);
					this->_node_alloc.deallocate(n, 1);
				}
			}

			/****************************** RBTREE CLEAR: *******************************/

			void rbtree_clear_all()
			{
				this->rbtree_clear(this->root);
				this->root = this->ghost;
				this->size = 0;
			}

			void node_delete(node_type *node)
			{
				this->_node_alloc.destroy(node);
				this->_node_alloc.deallocate(node, 1);
			}

			void rbtree_clear(node_type *node)
			{
				if (node == this->ghost)
					return;
				if (node->right != this->ghost)
					this->rbtree_clear(node->right);
				if (node->left != this->ghost)
					this->rbtree_clear(node->left);
				this->node_delete(node);
			}

			/***************************** RBTREE OPERATIONS: ****************************/

			iterator lower_bound(value_type const val)
			{
				iterator it = this->begin();
				iterator ite = this->end();
				for (; it != ite; ++it)
					if (this->_comp(*it, val) == false)
						break ;
				return (it);
			}

			const_iterator lower_bound(value_type const val) const
			{
				const_iterator it = this->begin();
				const_iterator ite = this->end();
				for (; it != ite; ++it)
					if (this->_comp(*it, val) == false)
						break ;
				return (it);
			}

			iterator upper_bound(value_type const val)
			{
				iterator it = this->begin();
				iterator ite = this->end();
				for (; it != ite; ++it)
					if (this->_comp(val, *it) == true)
						break ;
				return (it);
			}

			const_iterator upper_bound(value_type const val) const
			{
				const_iterator it = this->begin();
				const_iterator ite = this->end();
				for (; it != ite; ++it)
					if (this->_comp(val, *it) == true)
						break ;
				return (it);
			}

			node_type *min_node(node_type *n) const
			{
				if (n != this->ghost)
				{
					while (n && n->left != this->ghost)
						n = n->left;
				}
				return (n);
			}

			node_type *max_node(node_type *n) const
			{
				if (n != this->ghost)
				{
					while (n->right != this->ghost)
						n = n->right;
				}
				return (n);
			}

			/***************************** RBTREE CAPACITY: ****************************/

			//size_type const &min(size_type const &a, size_type const &b)
			//{
			//	return (a < b ? a : b);
			//}

			size_type	max_size() const
			{
				//std::cout << (size_type)std::numeric_limits<node_type>::max() << " " << std::numeric_limits<node_type>::max() / (sizeof(node_type) + sizeof(pointer)) << std::endl;
				//return (min((size_type)std::numeric_limits<difference_type>::max(), std::numeric_limits<size_type>::max() / (sizeof(node_type) + sizeof(pointer))));
				return ((allocator_type().max_size() * sizeof(value_type)) / sizeof(ft::rbtree_node<value_type>));
			}

	};
}

#endif
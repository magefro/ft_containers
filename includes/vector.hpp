//
// Created by antoine on 03/03/2022.
//

#ifndef FT_CONTAINERS_VECTOR_HPP
#define FT_CONTAINERS_VECTOR_HPP

#include <memory>
#include <iostream>
#include "reverse_iterator.hpp"
#include "vector_iterator.hpp"
#include "enable_if.hpp"
#include "is_integral.hpp"
#include "equal.hpp"
#include "lexicographical_compare.hpp"

namespace ft {

	template <typename T, typename Alloc = std::allocator<T> >
	class vector
	{
			/**************************** VECTOR TYPEDEFS: ****************************/

		public:

			typedef				T											value_type;
			typedef 			Alloc										allocator_type;
			typedef	typename 	allocator_type::reference					reference;
			typedef	typename 	allocator_type::const_reference				const_reference;
			typedef	typename 	allocator_type::pointer						pointer;
			typedef	typename 	allocator_type::const_pointer				const_pointer;
			typedef typename	ft::vector_iterator<value_type, false>		iterator;
			typedef typename	ft::vector_iterator<value_type const, true>	const_iterator;
			typedef typename	std::reverse_iterator<iterator>				reverse_iterator;
			typedef typename	std::reverse_iterator<const_iterator>		const_reverse_iterator;
			typedef				std::ptrdiff_t								difference_type;
			typedef				size_t										size_type;

			/**************************** VECTOR ATTRIBUTES: ****************************/

		private:

			size_type														_size;
			size_type														_capacity;
			allocator_type													_alloc;
			value_type														*_data;

			/**************************** VECTOR CONSTRUCTORS: ****************************/

		public:

			explicit vector (const allocator_type& alloc = allocator_type() ) :
				_size(0),
				_capacity(0),
				_alloc(alloc),
				_data(NULL)
			{
					return ;
			} //empty container constructor (default constructor) (1)
			//	Constructs an empty container, with no elements

			explicit vector (size_type n, const value_type& val = value_type(), const allocator_type& alloc = allocator_type()) :
				_size(n),
				_capacity(n),
				_alloc(alloc)
			{
					this->_data = this->_alloc.allocate(n);
					for (size_type idx = 0; idx < n; idx++)
						this->_alloc.construct(&this->_data[idx], val);
					return ;

			} //fill constructor (2)
			// 	Constructs a container with n elements. Each element is a copy of val.

			template <class InputIterator>
			vector (InputIterator first, InputIterator last, const allocator_type& alloc = allocator_type(),
			typename ft::enable_if<!(ft::is_integral<InputIterator>::value), InputIterator>::type* = NULL) :
				_alloc(alloc)
			{
				size_t len = this->_get_iterator_len(first, last);
				this->_data = this->_alloc.allocate(len);
				this->_size = len;
				this->_capacity = len;
				InputIterator	first_cpy = first;
				size_t idx = 0;
				while (first_cpy != last)
				{
					this->_alloc.construct(&this->_data[idx], *first_cpy);
					idx++;
					first_cpy++;
				}
			} //range constructor (3)
			//	Constructs a container with as many elements as the range (first, last), with each element constructed
			//from its corresponding element in that range, in the same order.

			vector (const vector &x) :
				_capacity(x._size),
				_alloc(x._alloc),
				_data(NULL)
			{
				this->_size = 0;
				this->_data = this->_alloc.allocate(x._size);
				for (size_type idx = 0; idx < x._size; idx++)
				{
					this->push_back(*(x._data + this->_size));
				}
			} //copy constructor (4)
			//	Constructs a container with a copy of each of the elements in x, in the same order.

            /**************************** VECTOR DESTRUCTORS: ****************************/

			virtual ~vector(void)
			{
				for (size_type idx = 0; idx < this->_size; idx++)
					this->_alloc.destroy(&this->_data[idx]);
				this->_size = 0;
				this->_alloc.deallocate(this->_data, this->_capacity);
				this->_data = NULL;
				this->_capacity = 0;
				return ;
			}

			/**************************** VECTOR ASSIGNMENT: ****************************/

			vector &operator=(const vector &v)
			{
				if (this != &v)
					this->assign(v.begin(), v.end());
				return (*this);
			}

			/**************************** VECTOR CAPACITY: ****************************/

			size_type	size(void) const
			{
				return (this->_size);
			}

			size_type	max_size(void) const
			{
				return (this->_alloc.max_size());
			}

			size_type	capacity(void) const
			{
				return (this->_capacity);
			}

			/* Resizes the container so that it contains n elements. */
			void	resize( size_type n, value_type val = value_type() )
			{
				/* If n is smaller than the current container size, the content is reduced to
				 * its first n elements, removing those beyond (and destroying them).
				 */
				if (n > this->max_size())// || n < 0)
				{
					throw std::length_error("length error");
				}
				if (n < this->_size)
				{
					for (size_type idx = n; idx < this->_size; idx++)
					{
						this->_alloc.destroy(&this->_data[idx]);
					}
					this->_size = n;
				}
					/* If n is greater than the current container size, the content is expanded by inserting
					 * at the end as many elements as needed to reach a size of n. If val is specified,
					 * the new elements are initialized as copies of val, otherwise, they are value-initialized.
					 * if n is also greater than the current container capacity, an automatic realocation of
					 * the allocated storage space takes place.
					 */
				else
				{
					if (n > this->_capacity && n <= this->_size * 2)
						reserve(this->_size * 2);
					else if (n > this->_capacity)
						reserve(n);
					for (size_type idx = this->_size; idx < n; idx++)
					{
						this->_alloc.construct(&this->_data[idx], val);
					}
					this->_size = n;
				}
			}

			/* Returns whether the vector is empty (size = 0). */
			bool	empty() const
			{
				if (this->_size == 0)
					return (true);
				return (false);
			}

			/* Requests that the vector capacity be at least enough to contain n elements. */
			void	reserve(size_type n)
			{
				/* If n is greater than the current vector capacity, the function causes the container
				 * to reallocate its storage increasing its capacity to n (or greater).
				 */
				if (n > this->max_size())// || n < 0)
				{
					throw std::length_error("vector::reserve");
				}
				if (n > this->_capacity)
				{
					allocator_type tmp_alloc;
					value_type *tmp_data = tmp_alloc.allocate(n);
					for (size_t i = 0; i < this->_size; i++)
					{
						tmp_alloc.construct(&tmp_data[i], this->_data[i]);
						this->_alloc.destroy(&this->_data[i]);
					}
					this->_alloc.deallocate(this->_data, this->_capacity);
					this->_alloc = tmp_alloc;
					this->_data = tmp_data;
					this->_capacity = n;
				}
			}

			/*************************** VECTOR ITERATOR FUNCTIONS: ***************************/

		public:

			iterator	begin(void)
			{
				return (iterator(this->_data));
			}

			iterator	end(void)
			{
				return (iterator(&this->_data[this->_size]));
			}

			const_iterator	begin(void) const
			{
				return (const_iterator(this->_data));
			}

			const_iterator	end(void) const
			{
				return (const_iterator(&this->_data[this->_size]));
			}

			reverse_iterator	rbegin(void)
			{
				return (reverse_iterator(&this->_data[this->_size]));
			}

			reverse_iterator	rend(void)
			{
				return (reverse_iterator(this->_data));
			}

			const_reverse_iterator	rbegin(void) const
			{
				return (const_reverse_iterator(&this->_data[this->_size]));
			}

			const_reverse_iterator	rend(void) const
			{
				return (const_reverse_iterator(this->_data));
			}

			/*************************** VECTOR ELEMENTS ACCESS: ***************************/


            reference operator[](size_type n)
            {
                return (this->_data[n]);
            }

            const_reference operator[](size_type n) const
            {
                return (this->_data[n]);
            }

            reference at(size_type idx)
			{
                if (idx >= this->_size)
                    throw std::out_of_range("Vector index out of range");
                return (this->_data[idx]);
            }

            const_reference at(size_type idx) const
			{
                if (idx >= this->_size)
					throw std::out_of_range("Vector index out of range");
                return (this->_data[idx]);
            }

            reference front(void)
			{
                return (this->_data[0]);
            }

            const_reference front(void) const
			{
                return (this->_data[0]);
            }

            reference back(void)
			{
                return (this->_data[this->_size - 1]);
            }

            const_reference back(void) const
			{
                return (this->_data[this->_size - 1]);
            }

			/****************************** VECTOR MODIFIERS: ******************************/

			/* Adds a new element at the end of the vector, after its current last element. The content of val is
			 * copied (or moved) to the new element */
			void	push_back(const value_type& val)
			{
				if (this->_size == this->_capacity)
					reserve((this->_size == 0) ? (1) : (this->_size * 2));
				this->_alloc.construct(&this->_data[this->_size], val);
				this->_size += 1;
			}
			/* This effectively increases the container size by one, which causes an automatic reallocation of
			 * the allocated storage of the allocated storage space if -and only if- the new vector size surpasses
			 * the current vector capacity */

            /* Assign new contents to the vector, replacing its current contents, and modifying its size accordingly. */

            /* In the range version the new contents are elements constructed from each of the elements in the range
             * between first and last, int the same order. */
			template <typename InputIterator>
            void    assign(typename ft::enable_if<!(ft::is_integral<InputIterator>::value),
					InputIterator>::type first, InputIterator last)
			{
				size_t len = this->_get_iterator_len(first, last);
				if (len > this->max_size())// || len < 0))
				{
					throw std::length_error("length error");
				}
				this->clear();
				this->insert(this->begin(), first, last);
            }

            /* In the fill version, the new contents are n elements, each initialized to a copy of val */
            void    assign(size_type n, const value_type& val)
			{
				if (n > this->max_size())// || n <= 0)
				{
					if (!this->_data)
						return ;
					else
					{
						this->clear();
						this->_alloc.deallocate(this->_data, this->_capacity);
						this->_data = NULL;
						this->_size = 0;
						this->_capacity = 0;
					}
					throw std::length_error("length error");
				}
				this->clear();
				this->insert(this->begin(), n, val);
            }

			/* Removes the last element in the vector, effictively reducing the container size by one
			 * This destroys the removed element
			 */
			void	pop_back(void)
			{
				if (this->_size > 0)
				{
					this->_size -= 1;
					this->_alloc.destroy(&this->_data[this->_size]);
				}
			}

            void    clear(void)
			{
                for (size_type idx = 0; idx < this->_size; idx++)
                    this->_alloc.destroy(&this->_data[idx]);
                this->_size = 0;
            }

			/* Erase Elements
			 * Removes from the vector either a single element (position) or a range of elements (first, last).
			 * This effectively reduces the container size by the number of elements removed, which are destroyed.
			 * Because vectors use an array as their underlying storage, erasing elements in positions other than
			 * the vector end causes the container to relocate all the elements after the segment erased to their
			 * new positions. This is generally an inefficient operation compared to the one performed for the same
			 * operation by other kinds of sequence containers (such as list or forward_list).
			 */
			iterator	erase(iterator first, iterator last)
			{
				size_t len = this->_get_iterator_len(first, last);
				iterator ret = first;
				if (first != last)
				{
					while (last != this->end())
					{
						*first = *last;
						first++;
						last++;
					}
					for (size_t i = len; i > 0; i--)
						this->_alloc.destroy(&this->_data[--this->_size]);
				}
				return (ret);
			}

			iterator	erase(iterator position)
			{
				return (this->erase(position, position + 1));
			}

			/* Insert Elements
			 * The vector is extended by inserting new elements before the element at the specified position, effectively
			 * increasing the container size by the number of elements inserted.
			 * This causes an automatic reallocation of the allocated storage space if -and only if- the new vector size
			 * suerpasses the current vector capacity.
			 * Because vectors use an array as their underlying storage, inserting elements in positions other than the
			 * vector end causes the container to relocate all the elements that were after position to their new positions.
			 * This is generally an inefficient operation compared to the one performed for the same operation by other
			 * kinds of sequence containers (such as list or forward_list),
			 * The parameters determine how many elements are inserted and to which values they are initialized.
			 */

			iterator	insert(iterator position, const value_type& val)
			{
				difference_type idx = position - this->begin();
				this->insert(position, 1, val);
				return (iterator(this->begin() + idx));
			}

			void	insert(iterator position, size_type n, const value_type& val)
			{
				if (n == 0)
					return ;
				iterator it = this->begin();
				if (this->_size + n >= this->_capacity * 2)
					reserve(this->_size + n);
				else if (this->_size + n >= this->_capacity && this->_size + n > this->_size * 2)
					reserve((this->_size + n));
				else if (this->_size + n >= this->_capacity)
					reserve((this->_size) * 2);
				size_type i = 0;
				while (it != position) {
					++it;
					++i;
				}
				for (size_type j = this->_size; j >= 1 && j > i; j--)
				{
					this->_alloc.construct(&this->_data[j + n - 1], this->_data[j - 1]);
					this->_alloc.destroy(&this->_data[j - 1]);
				}
				for (size_type j = 0; j < n; j++)
				{
					this->_alloc.construct(&this->_data[i + j], val);
					this->_size++;
				}
			}

			template <class InputIterator>
			typename ft::enable_if<!ft::is_integral<InputIterator>::value>::type
			insert(iterator position, InputIterator first, InputIterator last)
			{
				size_type n = this->_get_iterator_len(first, last);
				if (n == 0)
					return ;
				iterator it = this->begin();
				if (this->_size + n >= this->_capacity * 2)
					reserve(this->_size + n);
				else if (this->_size + n >= this->_capacity && this->_size + n > this->_size * 2)
					reserve((this->_size + n));
				else if (this->_size + n >= this->_capacity)
					reserve(this->_size * 2);
				size_type i = 0;
				while (it != position) {
					++it;
					++i;
				}
				for (size_type j = this->_size; j >= 1 && j > i; j--)
				{
					this->_alloc.construct(&this->_data[j + n - 1], this->_data[j - 1]);
					this->_alloc.destroy(&this->_data[j - 1]);
				}
				for (size_type j = 0; j < n; j++)
				{
					this->_alloc.construct(&this->_data[i + j], *first++);
					this->_size++;
				}
			}

			/* Swap content
			 * Exchanges the content of the container by the content of x, which is another vector object of
			 * the same type. Sizes may differ.
			 * After the call to this member function, the elements in this container are thoses which were
			 * in x before the call, and the elements of x are those which were in this. All iterators,
			 * references and pointers remain valid for the swapped objects.
			 * Notice that a non-member function exists with the same name, swap, overloading that algorithm
			 * with an optimization that behaves like this member function.
			 */
			void	swap(vector &x)
			{
				vector	tmp;
				tmp._copy_data(x);
				x._copy_data(*this);
				this->_copy_data(tmp);
			}

			/***************************** VECTOR ALLOCATORS: ******************************/

			allocator_type	get_allocator() const
			{
				return (this->_alloc);
			}

			/**************************** VECTOR PRIVATE FUNCTIONS: ****************************/

		private:

			void	_empty_container(void)
			{
				for (size_t idx = 0; idx < this->_size; idx++)
					this->_alloc.destroy(&this->_data[idx]);
				this->_size = 0;
				this->_alloc.deallocate(this->_data, this->_capacity);
				this->_data = NULL;
				this->_capacity = 0;
			}

			void	_copy_data(vector &x)
			{
				this->_data = x._data;
				this->_alloc = x._alloc;
				this->_size = x._size;
				this->_capacity = x._capacity;
				x._data = NULL;
				x._size = 0;
				x._capacity = 0;
			}

			template <typename InputIterator>
			size_t	_get_iterator_len(typename ft::enable_if<!(ft::is_integral<InputIterator>::value),
					InputIterator >::type first, InputIterator last)
			{
				size_t idx = 0;
				while (first != last)
				{
					++idx;
					++first;
				}
				return (idx);
			}

	};

	/********************************* VECTOR NON MEMBER: **********************************/

	/* The equality comparison (operator==) is erformed by first comparing sizes, and if they match, the elements
	 * are compared sequentially using operator==, stopping at the first mismatch (as if using algorithm equal.
	 */
	template <class T, class Alloc>
	bool operator==(const vector<T, Alloc> &lhs, const vector<T, Alloc> &rhs)
	{
		if (lhs.size() != rhs.size())
			return (false);
		return (ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
	}

	template <class T, class Alloc>
	bool operator!=(const vector<T, Alloc> &lhs, const vector<T, Alloc> &rhs)
	{
		return !(lhs == rhs);
	}

	/* The less-than comparison (operator<) behaves as if using algorithm lexicographical_compare which compares the
	 * elements sequentially using operator< in a reciprocal manner (i.e, checking both a<b and b<a) and stopping
	 * at the first occurence.
	 */

	template <class T, class Alloc>
	bool operator<(const vector<T, Alloc> &lhs, const vector<T, Alloc> &rhs)
	{
		return (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()));
	}

	template <class T, class Alloc>
	bool operator <=(const vector<T, Alloc> &lhs, const vector<T, Alloc> &rhs)
	{
		return !(rhs < lhs);
	}

	template <class T, class Alloc>
	bool operator >(const vector<T, Alloc> &lhs, const vector<T, Alloc> &rhs)
	{
		return (rhs < lhs);
	}

	template <class T, class Alloc>
	bool operator >=(const vector<T, Alloc> &lhs, const vector<T, Alloc> &rhs)
	{
		return !(lhs < rhs);
	}

	template <class T, class Alloc>
	void	swap(vector<T, Alloc> &x, vector<T, Alloc> &y)
	{
		x.swap(y);
	}
}

#endif //FT_CONTAINERS_VECTOR_HPP

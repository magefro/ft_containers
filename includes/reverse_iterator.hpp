//
// Created by alangloi on 4/25/22.
//

#ifndef FT_CONTAINERS_REVERSE_ITERATOR_HPP
#define FT_CONTAINERS_REVERSE_ITERATOR_HPP

#include "iterator_traits.hpp"

namespace ft {

	template <typename T>
	class reverse_iterator
	{
			/*************************** REVERSE ITERATOR TYPEDEFS: **************************/

		private:

			typedef iterator_traits<T> 					_traits;

		public:

			typedef typename _traits::value_type		value_type;
			typedef typename _traits::pointer			pointer;
			typedef typename _traits::reference			reference;
			typedef typename _traits::difference_type	difference_type;
			typedef T									iterator;

			/*************************** REVERSE ITERATOR ATTRIBUTES: **************************/

			T	ptr;

		public:

			/*************************** REVERSE ITERATOR CONSTRUCTORS: **************************/

			reverse_iterator() : ptr() {}
			reverse_iterator(iterator it): ptr(it) {}
			template <typename U>
			reverse_iterator(reverse_iterator<U> const &it) : ptr(it.ptr) {}

			/*************************** REVERSE ITERATOR DESTRUCTORS: **************************/

			virtual ~reverse_iterator(void) {};

			/*********************** REVERSE ITERATOR ASSIGNMENT OPERATOR: **********************/

			reverse_iterator &operator=(reverse_iterator const &it)
			{
				if (*this != it)
					this->ptr = it.base();
				return (*this);
			}

			/*********************** REVERSE ITERATOR ASSIGNMENT OPERATOR: **********************/

			iterator	base(void) const
			{
				return (this->ptr);
			}

			/*************************** REVERSE ITERATOR ACCESSORS: **************************/

			reference	operator*(void) const
			{
				iterator ite(this->ptr);
				return (*--ite);
			}

			pointer		operator->(void) const
			{
				return (&this->operator*());
			}

			/*************************** REVERSE ITERATOR OPERATORS: **************************/

			reverse_iterator	operator+(difference_type n) const
			{
				return (reverse_iterator(this->ptr - n));
			}

			difference_type		operator+(const reverse_iterator &u) const
			{
				return (this->ptr + u.ptr);
			}

			reverse_iterator	operator-(difference_type n) const
			{
				return (reverse_iterator(this->ptr + n));
			}

			difference_type		operator-(const reverse_iterator &u) const
			{
				return (this->ptr - u.ptr);
			}

			reverse_iterator	&operator++(void)
			{ //pre-increment
				this->ptr.operator--();
				return (*this);
			}

			reverse_iterator	operator++(int)
			{ //post-increment
				reverse_iterator tmp(*this);
				this->ptr.operator--();
				return (tmp);
			}

			reverse_iterator	&operator--(void)
			{ //pre-decrement
				this->ptr.operator++();
				return (*this);
			}

			reverse_iterator	operator--(int)
			{ //post-decrement
				reverse_iterator	tmp(*this);
				this->ptr.operator++();
				return (tmp);
			}

			reverse_iterator	&operator+=(difference_type it)
					{
				this->ptr.operator-=(it);
				return (*this);
			}

			reverse_iterator	&operator-=(difference_type it)
			{
				this->ptr.operator+=(it);
				return (*this);
			}

			bool operator<(reverse_iterator &ref)
			{
				return (this->ptr > ref.ptr);
			}

			bool operator>(reverse_iterator &ref)
			{
				return (this->ptr < ref.ptr);
			}

			bool operator<=(reverse_iterator &ref)
			{
				return (this->ptr > ref.ptr);
			}

			bool operator>=(reverse_iterator &ref)
			{
				return (this->ptr < ref.ptr);
			}

			reference operator[](difference_type n) const
			{
				return (*(this->ptr - n - 1));
			}

	};

	/************************** REVERSE ITERATOR NON MEMBER: ******************************/

	template <class iterator>
	reverse_iterator<iterator>	operator+(typename reverse_iterator<iterator>::difference_type n, const reverse_iterator<iterator>& rev_it)
	{
		return (reverse_iterator<iterator>(rev_it.base() - n));
	}

	template <class iterator>
	typename reverse_iterator<iterator>::difference_type operator-(const reverse_iterator<iterator>& lhs, const reverse_iterator<iterator>& rhs)
	{
		return (lhs.base() - rhs.base());
	}

	template <typename Iter1, typename Iter2>
	bool operator==(reverse_iterator<Iter1> const &lhs, reverse_iterator<Iter2> const &rhs)
	{
		return (lhs.base() == rhs.base());
	}

	template <typename Iter1, typename Iter2>
	bool operator!=(reverse_iterator<Iter1> const &lhs, reverse_iterator<Iter2> const &rhs)
	{
		return (lhs.base() != rhs.base());
	}

	template <typename Iter1, typename Iter2>
	bool operator<(reverse_iterator<Iter1> const &lhs, reverse_iterator<Iter2> const &rhs)
	{
		return (lhs.base() > rhs.base());
	}

	template <typename Iter1, typename Iter2>
	bool operator>(reverse_iterator<Iter1> const &lhs, reverse_iterator<Iter2> const &rhs)
	{
		return (lhs.base() > rhs.base());
	}

	template <typename Iter1, typename Iter2>
	bool operator<=(reverse_iterator<Iter1> const &lhs, reverse_iterator<Iter2> const &rhs)
	{
		return (lhs.base() >= rhs.base());
	}

	template <typename Iter1, typename Iter2>
	bool operator>=(reverse_iterator<Iter1> const &lhs, reverse_iterator<Iter2> const &rhs)
	{
		return (lhs.base() >= rhs.base());
	}

}

#endif //FT_CONTAINERS_REVERSE_ITERATOR_HPP

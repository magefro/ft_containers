//
// Created by alangloi on 5/3/22.
//

#ifndef FT_CONTAINERS_MAKE_PAIR_HPP
#define FT_CONTAINERS_MAKE_PAIR_HPP

#include "pair.hpp"

namespace ft {

	template <class T1, class T2>
	ft::pair<T1, T2> make_pair (T1 x, T2 y)
	{
		return ( ft::pair<T1, T2>(x, y) );
	}

}

#endif //FT_CONTAINERS_MAKE_PAIR_HPP

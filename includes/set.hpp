//
// Created by alangloi on 6/14/22.
//

#ifndef FT_CONTAINERS_SET_HPP
#define FT_CONTAINERS_SET_HPP

#include "rbtree.hpp"
#include "equal.hpp"
#include "lexicographical_compare.hpp"

namespace ft {

	template <	class T,							//set::key_type/value_type
				class Compare = std::less<T>,		//set::key_compare/value_compare
				class Alloc = std::allocator<T> >	//set::allocatory_type
	class set
	{
			/******************************* SET TYPEDEFS: *******************************/

		public:

			typedef T 																key_type;
			typedef T 																value_type; //identic
			typedef Compare 														key_compare;
			typedef Compare 														value_compare; //identic
			typedef Alloc															allocator_type;
			typedef typename allocator_type::reference								reference;
			typedef typename allocator_type::const_reference						const_reference;
			typedef typename allocator_type::pointer								pointer;
			typedef typename allocator_type::const_pointer							const_pointer;
			typedef std::ptrdiff_t													difference_type;
			typedef size_t															size_type;

			//enum 	rbtree_node_color { RED, BLACK };

		protected:

			typedef ft::rbtree_node<const value_type> 								node_type;
			typedef ft::rbtree<const value_type, node_type, value_compare, Alloc>	tree;

		public:

			typedef ft::rbtree_iterator<const value_type, node_type>				iterator; //iterators are always const in set since values can't be modified
			typedef ft::rbtree_iterator<const value_type, node_type>				const_iterator;
			typedef ft::reverse_iterator<iterator> 									reverse_iterator;
			typedef ft::reverse_iterator<const_iterator> 							const_reverse_iterator;

			/****************************** SET ATTRIBUTES: ******************************/

		private:

			allocator_type															_alloc;
			key_compare																_comp;
			std::allocator<node_type>												_node_alloc;
			tree																	_tree;

		public:

			/***************************** SET CONSTRUCTORS: *****************************/

			set (const key_compare& comp = key_compare(), const allocator_type& alloc = allocator_type()) :
						  _alloc(alloc),
						  _comp(comp),
						  _tree(comp, alloc)
			{
				return ;
			}

			template <class InputIterator>
			set (InputIterator first, InputIterator last, const key_compare& comp = key_compare(),
				 const allocator_type& alloc = allocator_type()) :
				 _alloc(alloc),
				 _comp(comp),
				 _tree(comp, alloc)
			 {
				this->insert(first, last);
			}

			set (const set &x) :
				_alloc(x._alloc),
				_comp(x._comp),
				_tree(x._comp, x._alloc)
			{
				*this = x;
			}

			/***************************** SET DESTRUCTORS: *****************************/

			virtual ~set(void) {};

			/************************* SET ASSIGNMENT OPERATOR: *************************/

			set& operator=(const set &x)
			{
				if (this != &x)
				{
					this->clear();
					this->insert(x.begin(), x.end());
				}
				return (*this);
			}

			/****************************** SET ITERATORS: ******************************/

			iterator begin()
			{
				return (this->_tree.begin());
			}

			const_iterator begin() const
			{
				return (this->_tree.begin());
			}

			iterator end()
			{
				return (this->_tree.end());
			}

			const_iterator end() const
			{
				return (this->_tree.end());
			}

			reverse_iterator rbegin()
			{
				return (this->_tree.rbegin());
			}

			const_reverse_iterator rbegin() const
			{
				return (this->_tree.rbegin());
			}

			reverse_iterator rend()
			{
				return (this->_tree.rend());
			}

			const_reverse_iterator rend() const
			{
				return (this->_tree.rend());
			}

			/****************************** SET CAPACITY: ******************************/

			bool empty() const
			{
				if (this->_tree.size == 0)
				{
					return (true);
				}
				else
				{
					return (false);
				}

			}

			size_type size() const
			{
				return (this->_tree.size);
			}

			size_type max_size() const
			{
				return (this->_node_alloc.max_size());
			}

			/***************************** SET MODIFIERS: *****************************/

			pair<iterator,bool> insert(const value_type& val)
			{
				return (this->_tree.rbtree_insert(this->end(), val));
			}

			iterator insert(iterator position, const value_type& val)
			{
				return (this->_tree.rbtree_insert(position, val).first);
			}

			template <class InputIterator>
			void insert(InputIterator first, InputIterator last)
			{
				while (first != last)
				{
					this->insert(*first);
					first++;
				}
			}

			void erase(iterator position)
			{
				this->_tree.rbtree_delete(*position);
			}

			size_type erase(const value_type& val)
			{
				size_t ret = this->count(val);
				if (ret > 0)
					this->_tree.rbtree_delete(this->find(val).node_ptr()->val);
				return (ret);
			}

			void erase(iterator first, iterator last)
			{
				iterator it = first;
				while (first != last)
				{
					it++;
					this->_tree.rbtree_delete(*first);
					first = it;
				}
			}

			void swap(set& x)
			{
				set tmp;
				tmp._copy_content_set(x);
				x._copy_content_set(*this);
				this->_copy_content_set(tmp);
			}

			void clear()
			{
				this->_tree.rbtree_clear_all();
			}

			/***************************** SET MODIFIERS: *****************************/

			key_compare key_comp() const
			{
				return (key_compare());
			}

			value_compare value_comp() const
			{
				return (value_compare(key_compare()));
			}

			/**************************** SET OPERATIONS: ****************************/

			iterator find(const value_type& val)
			{
				return (this->_tree.rbtree_lookup(val));
			}

			const_iterator find(const value_type& val) const
			{
				return (this->_tree.rbtree_lookup(val));
			}

			size_type count(const value_type& val) const
			{
				return (this->_tree.rbtree_lookup(val) != this->end());
			}

			iterator lower_bound(const value_type& val)
			{
				return (this->_tree.lower_bound(val));
			}

			const_iterator lower_bound(const value_type& val) const
			{
				return (this->_tree.lower_bound(val));
			}

			iterator upper_bound(const value_type& val)
			{
				return (this->_tree.upper_bound(val));
			}

			const_iterator upper_bound(const value_type& val) const
			{
				return (this->_tree.upper_bound(val));
			}

			pair<iterator,iterator> equal_range(const value_type& val)
			{
				pair<iterator, iterator> ret;
				ret.first = this->lower_bound(val);
				ret.second = this->upper_bound(val);
				return (ret);
			}

			pair<const_iterator, const_iterator> equal_range(const value_type& val) const
			{
				pair<const_iterator, const_iterator> ret;
				ret.first = this->lower_bound(val);
				ret.second = this->upper_bound(val);
				return (ret);
			}

			/***************************** SET ALLOCATOR: *****************************/

			allocator_type get_allocator() const
			{
				return (this->_alloc);
			}

			/************************* SET PRIVATE FUNCTIONS: *************************/

		private:

			void _copy_content_set(set &x)
			{
				this->clear();
				node_type *tmp_root = this->_tree.root;
				node_type *tmp_ghost = this->_tree.ghost;
				this->_alloc = x._alloc;
				this->_comp = x._comp;
				this->_node_alloc = x._node_alloc;
				this->_tree.size = x._tree.size;
				this->_tree.root = x._tree.root;
				this->_tree.ghost = x._tree.ghost;
				x._tree.root = tmp_root;
				x._tree.ghost = tmp_ghost;
				x._tree.size = 0;
			}
	};

	/*********************** SET RELATIONNAL OPERATORS: ************************/

	template <class T, class Compare, class Alloc>
	bool operator==(const set<T, Compare, Alloc> &lhs, const set<T, Compare, Alloc> &rhs)
	{
		if (lhs.size() != rhs.size())
			return (false);
		return (ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
	}

	template <class T, class Compare, class Alloc>
	bool operator!=(const set<T, Compare, Alloc> &lhs, const set<T, Compare, Alloc> &rhs)
	{
		if (lhs == rhs)
			return (false);
		return (true);
	}

	template <class T, class Compare, class Alloc>
	bool operator<(const set<T, Compare, Alloc> &lhs, const set<T, Compare, Alloc> &rhs)
	{
		return (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()));
	}

	template <class T, class Compare, class Alloc>
	bool operator<=(const set<T, Compare, Alloc> &lhs, const set<T, Compare, Alloc> &rhs)
	{
		return !(rhs < lhs);
	}

	template <class T, class Compare, class Alloc>
	bool operator>(const set<T, Compare, Alloc> &lhs, const set<T, Compare, Alloc> &rhs)
	{
		return (rhs < lhs);
	}

	template <class T, class Compare, class Alloc>
	bool operator>=(const set<T, Compare, Alloc> &lhs, const set<T, Compare, Alloc> &rhs)
	{
		return !(lhs < rhs);
	}

}

#endif //FT_CONTAINERS_SET_HPP

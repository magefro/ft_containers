//
// Created by antoine on 03/03/2022.
//

#ifndef FT_CONTAINERS_STACK_HPP
#define FT_CONTAINERS_STACK_HPP

#include "vector.hpp"

namespace ft {

	template <typename T, class Container = ft::vector<T> >
	class stack
	{
			/**************************** STACK TYPEDEFS: ****************************/

		public:

			typedef T			value_type;
			typedef Container	container_type;
			typedef std::size_t		size_type;

			/*************************** STACK ATTRIBUTES: ***************************/

			container_type		_container;

			/*************************** STACK CONSTRUCTORS: ***************************/

		public:

			stack (const container_type& ctnr = container_type()) :
				_container(ctnr)
			{};

			/*************************** STACK DESTRUCTORS: ***************************/

			virtual ~stack(void) {};

			/*************************** STACK FUNCTIONS: ***************************/

			bool empty() const
			{
				return (this->_container.empty());
			}

			size_type size() const
			{
				return (this->_container.size());
			}

			value_type& top()
			{
				return (this->_container.back());
			}

			const value_type& top() const
			{
				return (this->_container.back());
			}

			void push(const value_type& val)
			{
				return (this->_container.push_back(val));
			}

			void pop()
			{
				return (this->_container.pop_back());
			}

	};

	/*************************** STACK NON MEMBER: ***************************/

	template <class T, class Container>
	bool operator==(const stack<T, Container> &lhs, const stack<T, Container> &rhs)
	{
		return (lhs._container == rhs._container);
	}

	template <class T, class Container>
	bool operator!=(const stack<T, Container> &lhs, const stack<T, Container> &rhs)
	{
		return !(lhs == rhs);
	}

	template <class T, class Container>
	bool operator<(const stack<T, Container> &lhs, const stack<T, Container>& rhs)
	{
		return (lhs._container < rhs._container);
	}

	template <class T, class Container>
	bool operator<=(const stack<T, Container> &lhs, const stack<T, Container>& rhs)
	{
		return !(rhs < lhs);
	}

	template <class T, class Container>
	bool operator>(const stack<T, Container> &lhs, const stack<T, Container>& rhs)
	{
		return (rhs < lhs);
	}

	template <class T, class Container>
	bool operator>=(const stack<T, Container> &lhs, const stack<T, Container>& rhs)
	{
		return !(lhs < rhs);
	}

}



#endif //FT_CONTAINERS_STACK_HPP

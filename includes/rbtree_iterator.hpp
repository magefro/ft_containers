//
// Created by alangloi on 5/10/22.
//

#ifndef FT_CONTAINERS_RBTREE_ITERATOR_HPP
#define FT_CONTAINERS_RBTREE_ITERATOR_HPP

#include "iterator_traits.hpp"

namespace ft {


	template <typename type_ite, typename node_type>
	class rbtree_iterator
	{
			/*************************** RBTREE ITERATOR TYPEDEFS: ***************************/

		private:

			typedef typename ft::iterator_traits<type_ite *>	_traits;

		public:

			typedef typename _traits::value_type				value_type;
			typedef typename _traits::pointer					pointer;
			typedef typename _traits::reference					reference;
			typedef typename _traits::reference const			const_reference;
			typedef typename _traits::pointer	const			const_pointer;
			typedef typename _traits::difference_type			difference_type;
			typedef std::bidirectional_iterator_tag				iterator_category;

			/*************************** RBTREE ITERATOR ATTRIBUTES: ***************************/

			node_type*											ptr;
			node_type*											root;
			node_type*											ghost;

		public:

			/*************************** RBTREE ITERATOR CONSTRUCTORS: ***************************/

			rbtree_iterator(void) : ptr(NULL) , root(NULL), ghost(NULL) {};
			rbtree_iterator(node_type* ptr, node_type* root, node_type *ghost) : ptr(ptr), root(root), ghost(ghost) {};
			rbtree_iterator(const rbtree_iterator &it) : ptr(it.ptr), root(it.root), ghost(it.ghost) {};

			/*************************** RBTREE ITERATOR DESTRUCTORS: ***************************/

			~rbtree_iterator() {};

			/******************** RBTREE ITERATOR CONST CONVERSION OPERATOR: ********************/

			operator	rbtree_iterator<const type_ite, node_type>(void) const
			{
				return (rbtree_iterator<const type_ite, node_type>(this->ptr, this->root, this->ghost));
			}

			/************************ RBTREE ITERATOR ASSIGNMENT OPERATOR: ***********************/

			rbtree_iterator &operator=(const rbtree_iterator &mi)
			{
				if (this != &mi)
				{
					this->ptr = mi.ptr;
					this->root = mi.root;
					this->ghost = mi.ghost;
				}
				return (*this);
			};

			/***************************** RBTREE ITERATOR CAPACITY: *****************************/

			node_type	*node_ptr(void) const
			{
				return (this->ptr);
			}

			pointer const &base(void) const
			{
				return (this->node_ptr());
			}

			/************************* RBTREE ITERATOR ELEMENT ACCESS: **************************/

			reference	operator*() const
			{
				return (this->ptr->val);
			}

			//const_reference operator*() const
			//{
			//	return (this->ptr->val);
			//}

			pointer 	operator->() const
			{
				return (&this->operator*());
			}

			//const_pointer 	operator->() const
			//{
			//	return (&this->operator*());
			//}

			/*********************** RBTREE ITERATOR OPERATIONS: ************************/

			node_type *min_node(node_type *node)
			{
				if (node != this->ghost)
				{
					while (node && node->left != this->ghost)
						node = node->left;
				}
				return (node);
			}

			node_type *max_node(node_type *node)
			{
				if (node != this->ghost)
				{
					while (node && node->right != this->ghost)
						node = node->right;
				}
				return (node);
			}

		/********************** RBTREE ITERATOR INCREMENT/DECREMENT OPERATORS: ***********************/

		rbtree_iterator &operator++(void)
			{ // pre increment
				if (this->ptr->right != this->ghost)
				{
					this->ptr = min_node(this->ptr->right);
				}
				else
				{
					node_type *tmp = this->ptr;
					this->ptr = this->ptr->parent;
					while (this->ptr != this->ghost && tmp == this->ptr->right) //should segfault if done on last element!!
					{
						tmp = this->ptr;
						this->ptr = this->ptr->parent;
					}
				}
				return (*this);
			}

			rbtree_iterator operator++(int)
			{ //post increment
				rbtree_iterator tmp(*this);
				++(*this);
				return (tmp);
			}

			rbtree_iterator &operator--(void)
			{ // pre decrement
				if (this->ptr == this->ghost)
					this->ptr = this->max_node(this->root);
				else
				{
					if (this->ptr->left != this->ghost)
						this->ptr = max_node(this->ptr->left);
					else
					{
						node_type *tmp = this->ptr;
						this->ptr = this->ptr->parent;
						while (this->ptr != this->ghost && tmp == this->ptr->left) // should segfault if done on last element!!
						{
							tmp = this->ptr;
							this->ptr = this->ptr->parent;
						}
					}
				}
				return (*this);
			}

			rbtree_iterator operator--(int)
			{ // post decrement
				rbtree_iterator tmp(*this);
				--(*this);
				return (tmp);
			}

			/********************** RBTREE ITERATOR RELATIONAL OPERATORS: ***********************/

			bool	operator==(const rbtree_iterator &rhs) const
			{
				return (this->ptr == rhs.ptr);
			}

			bool	operator!=(const rbtree_iterator &rhs) const
			{
				return (this->ptr != rhs.ptr);
			}
	};
}

#endif //FT_CONTAINERS_RBTREE_ITERATOR_HPP

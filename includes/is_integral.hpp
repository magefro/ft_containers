//
// Created by alangloi on 5/3/22.
//

#ifndef FT_CONTAINERS_IS_INTEGRAL_HPP
#define FT_CONTAINERS_IS_INTEGRAL_HPP

namespace ft {

	/* Trait class that identifies whether T is an integral type.
	 * It intherits from integral_constant as being either true_type or false_type,
	 * depending on whether T is an integral type : */
	template <class T, T v>
	struct integral_constant {
		static const T					value = v;
		typedef T						value_type;
		typedef integral_constant<T, v>	type;
		operator T() { return v; }
	};

	typedef integral_constant<bool, true>		true_type;
	typedef integral_constant<bool, false>		false_type;

	template <typename> struct	_is_integral_helper :						public false_type {};
	template<> struct			_is_integral_helper<bool> :					public true_type {};
	template<> struct			_is_integral_helper<char> :					public true_type {};
	template<> struct			_is_integral_helper<signed char> :			public true_type {};
	template<> struct			_is_integral_helper<wchar_t> :				public true_type {};
	template<> struct			_is_integral_helper<short> :				public true_type {};
	template<> struct			_is_integral_helper<unsigned short> :		public true_type {};
	template<> struct			_is_integral_helper<int> :					public true_type {};
	template<> struct			_is_integral_helper<unsigned int> :			public true_type {};
	template<> struct			_is_integral_helper<long> :					public true_type {};
	template<> struct			_is_integral_helper<unsigned long> :		public true_type {};
	template<> struct			_is_integral_helper<long long> :			public true_type {};
	template<> struct			_is_integral_helper<unsigned long long> :	public true_type {};

	template<typename T> struct	remove_const { 					typedef T	type; };
	template<typename T> struct	remove_const<T const> {			typedef T	type; };
	template<typename T> struct	remove_volatile {				typedef T	type; };
	template<typename T> struct	remove_volatile<T volatile> {	typedef T	type; };

	template<typename T>
	struct	remove_cv {
		typedef typename		remove_const<typename remove_volatile<T>::type>::type type;
	};

	template<typename T>
	struct	is_integral :
			public integral_constant<bool, (_is_integral_helper<typename remove_cv<T>::type>::value)> {};

};

#endif //FT_CONTAINERS_IS_INTEGRAL_HPP

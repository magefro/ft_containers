FT_NAME = ft_containers
STD_NAME = std_containers
CXX = c++
CXXFLAGS = -Wall -Wextra -Werror -std=c++98 -g3
INCLUDES_PATH = includes/
FLAG_STD = -DNAMESPACE=std
FLAG_FT = -DNAMESPACE=ft
SOURCES = main.cpp
FT_OBJ_PATH = ft_obj/
STD_OBJ_PATH = std_obj/
FT_OBJECTS = $(addprefix $(FT_OBJ_PATH), $(SOURCES:.cpp=.o))
STD_OBJECTS = $(addprefix $(STD_OBJ_PATH), $(SOURCES:.cpp=.o))
FT_OUTPUT = ft_containers.txt
STD_OUTPUT = std_containers.txt

all: $(FT_OBJ_PATH) $(FT_NAME) $(STD_OBJ_PATH) $(STD_NAME)

$(FT_NAME): $(FT_OBJECTS)
	$(CXX) $(CXXFLAGS) -o $@ $^ -I $(INCLUDES_PATH)
	@touch $(FT_OUTPUT)
	@echo "   ▄████████     ███       " >> ft_containers.txt
	@echo "  ███    ███ ▀█████████▄  " >> ft_containers.txt
	@echo "  ███    █▀     ▀███▀▀██  " >> ft_containers.txt
	@echo " ▄███▄▄▄         ███   ▀ " >> ft_containers.txt
	@echo "▀▀███▀▀▀         ███    " >> ft_containers.txt
	@echo "  ███            ███      " >> ft_containers.txt
	@echo "  ███            ███      " >> ft_containers.txt
	@echo "  ███           ▄████▀    " >> ft_containers.txt
	@echo "" >> ft_containers.txt

$(STD_NAME): $(STD_OBJECTS)
	$(CXX) $(CXXFLAGS) -o $@ $^ -I $(INCLUDES_PATH)
	@touch $(STD_OUTPUT)
	@echo "  ▄████████     ███     ████████▄  " >> std_containers.txt
	@echo "  ███    ███ ▀█████████▄ ███   ▀███" >> std_containers.txt
	@echo "  ███    █▀     ▀███▀▀██ ███    ███" >> std_containers.txt
	@echo "  ███            ███   ▀ ███    ███" >> std_containers.txt
	@echo "▀███████████     ███     ███    ███" >> std_containers.txt
	@echo "         ███     ███     ███    ███" >> std_containers.txt
	@echo "   ▄█    ███     ███     ███   ▄███" >> std_containers.txt
	@echo " ▄████████▀     ▄████▀   ████████▀ " >> std_containers.txt
	@echo "" >> std_containers.txt

${FT_OBJ_PATH}:
	mkdir -p $@

${STD_OBJ_PATH}:
	mkdir -p $@

${STD_OBJ_PATH}%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $< -I$(INCLUDES_PATH) $(FLAG_STD)

${FT_OBJ_PATH}%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $< -I$(INCLUDES_PATH) $(FLAG_FT)

clean:
	rm -rf $(FT_OBJ_PATH) $(STD_OBJ_PATH) $(STD_OUTPUT) $(FT_OUTPUT)

fclean: clean
	rm -rf $(FT_NAME) $(STD_NAME)

re: fclean all

.PHONY: all ft std fclean clean re